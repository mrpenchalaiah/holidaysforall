angular.module('firstApp')
	.factory('HotelPricing', function () {
		return function HotelPricing(hotelprice) {
            console.log('i came here');
            console.log(hotelprice);
			angular.extend(this, hotelprice);
		};
	})
	.factory('HotelPricingService', function ($q, $http, HotelPricing) {

		var hotelprice = [];
		var loadPromise = undefined;
		var loadAll = function () {
			if (hotelprice.loadPromise > 0) {
				return $q.when(hotelprice);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/rate/hotel/detail?checkInDate=05-09-2018&checkOutDate=06-09-2018&rooms%5B0%5D.adults=2&city=Bangalore&rooms%5B1%5D.adults=3&hotelid=00012864&ratePlanId=0001312515&roomTypeId=0000045636&rooms%5B0%5D.childAges=5,10&rooms%5B0%5D.childs=2',
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
                        console.log(data.data.data);
						hotelprice = new HotelPricing(data.data.data);
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function () {
				return loadAll().then(function () {
					return hotelprice;
				});
			}
		};

	});