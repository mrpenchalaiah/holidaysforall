angular.module('firstApp')
	.factory('Hotel', function () {
		return function Hotel(hotel) {
			angular.extend(this, hotel);
			console.log(hotel);
		};
	})
	.factory('CityhotelsService', function ($q, $http, Hotel) {

		var cityhotels = [];
		var loadPromise = undefined;
		var loadAll = function (cityname) {
			if (cityhotels.loadPromise > 0) {
				return $q.when(cityhotels);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/content/city?cityName='+ cityname.toUpperCase(),
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
						if(data.data.data) {
							cityhotels = data.data.data.map(function (hotel) {
								return new Hotel(hotel);
							});
						}
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function (cityname) {
				return loadAll(cityname).then(function () {
					return cityhotels;
				});
			}
		};

	});