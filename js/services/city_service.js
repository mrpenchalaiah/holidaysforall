angular.module('firstApp')
	.factory('City', function () {
		return function City(city) {
			angular.extend(this, city);
		};
	})
	.factory('CityService', function ($q, $http, City) {

		var cities = [];
		var loadPromise = undefined;
		var loadAll = function () {
			if (cities.loadPromise > 0) {
				return $q.when(cities);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/cities',
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
						cities = data.data.data.map(function (city) {
							return new City(city);
						});
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function () {
				return loadAll().then(function () {
					return cities;
				});
			}
		};

	});