angular.module('firstApp')
	.factory('Hotel', function () {
		return function Hotel(hotel) {
			angular.extend(this, hotel);
			console.log(hotel);
		};
	})
	.factory('HotelService', function ($q, $http, Hotel) {

		var hotel = [];
		var loadPromise = undefined;
		var loadAll = function (hotelid) {
			if (hotel.loadPromise > 0) {
				return $q.when(hotel);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/content/hotel?hotelid='+ hotelid,
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
						hotel = new Hotel(data.data.data);
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function (hotelid) {
				return loadAll(hotelid).then(function () {
					return hotel;
				});
			}
		};

	});