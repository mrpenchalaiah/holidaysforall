
angular.module('firstApp')
	.directive('home', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'homeCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/home.html'
		};
	});