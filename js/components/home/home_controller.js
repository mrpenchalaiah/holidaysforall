angular.module('firstApp')
	.controller('homeCtrl', function (CityService, $routeParams, $window, $scope) {
		var ctrl = this;
		ctrl.t = {
			title: 'Home page',
		};
		ctrl.cities = [];
		ctrl.datePicker = {
			startDate: null,
			endDate: null
		};

		ctrl.agelist = [{
			label: '1',
			value: '1'
		},
		{
			label: '2',
			value: '2'
		},
		{
			label: '3',
			value: '3'
		},
		{
			label: '4',
			value: '4'
		},
		{
			label: '5',
			value: '5'
		},
		{
			label: '6',
			value: '6'
		},
		{
			label: '7',
			value: '7'
		},
		{
			label: '8',
			value: '8'
		},
		{
			label: '9',
			value: '9'
		},
		{
			label: '10',
			value: '10'
		},
		{
			label: '11',
			value: '11'
		},
		{
			label: '12',
			value: '12'
		},
		];

		ctrl.datePickerOptins = {
			locale: {
				applyLabel: 'Apply',
				fromLabel: 'Check In',
				///format: 'YYYY-MM-DD', //will give you 2017-01-06
				//format: 'D-MMM-YY', //will give you 6-Jan-17
				format: 'DD-MM-YYYY', //will give you 6-January-17
				toLabel: 'Check Out',
				cancelLabel: 'Cancel',
				separator: ' to '
				//customRangeLabel: 'Custom range'
			}
		};

		ctrl.homebooknow = {
			type: 'hotel',
			location: 'BANGALORE',
			checkin: new Date(),
			checkout: new Date(new Date().setDate(new Date().getDate() + 10)),
			rooms: [{
				adults: 1,
				childs: 0,
				childAges: [],
			}],
			// adults: 1,
			// child: 0,
			// childAge: [],
		};

		CityService.getAll().then(function (cities) {
			ctrl.cities = cities;
			console.log(ctrl.cities);
		});

		ctrl.updateLocation = function () {
			console.log(ctrl.location);
			ctrl.homebooknow.location = ctrl.location.ca;
		};

		ctrl.addMore = function (type, index) {
			console.log(index);
			if (type == 'rooms') {
				ctrl.homebooknow.rooms.push({
					adults: 1,
					childs: 0,
					childAges: [],
				});
			} else if (type == 'adults') {
				console.log('i came here');
				console.log(type)
				console.log(index)
				console.log(ctrl.homebooknow.rooms[index]);
				if (parseInt(ctrl.homebooknow.rooms[index][type]) > 0) {
					console.log('console.log')
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] + 1);
				}
			} else if (type == 'childs') {
				if (parseInt(ctrl.homebooknow.rooms[index][type]) >= 0) {
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] + 1);
					ctrl.homebooknow.rooms[index].childAges.push({
						age: '1'
					});
				}
			}
		};
		ctrl.removeSome = function (type, index) {
			console.log(index);
			if (type == 'rooms') {
				if (!(parseInt(ctrl.homebooknow[type]) <= 1)) {
					ctrl.homebooknow.rooms = ctrl.homebooknow.rooms.slice(0, -1);
				}
			} else if (type == 'adults') {
				if (!(parseInt(ctrl.homebooknow.rooms[index][type]) <= 1)) {
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] - 1);
				}
			} else if (type == 'childs') {
				if (!(parseInt(ctrl.homebooknow.rooms[index][type]) <= 0)) {
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] - 1);
					console.log('removing part');
					console.log(ctrl.homebooknow.childAges);
					ctrl.homebooknow.rooms[index].childAges = ctrl.homebooknow.rooms[index].childAges.slice(0, -1);
				}
			}
		};

		ctrl.booknow = function () {
			console.log('************************************************************************');
			console.log(ctrl.homebooknow);
			var checkindate = moment(ctrl.homebooknow.checkin).format('DD-MM-YYYY');
			var checkoutdate = moment(ctrl.homebooknow.checkout).format('DD-MM-YYYY');


			ctrl.homebooknow.rooms.map(function (item, index) {
				console.log(item);
				var str = '';
				for (var key in item) {
					console.log(key, 'key')
					if (str != '') {
						str += '&';
					}
					if (key == 'childAges') {
						var childage = [];
						item[key].map(function (item) {
							childage.push(item.age);
						});
						console.log(childage);
						str += encodeURIComponent(`room[${index}].`) + key + '=' + childage.toString();
					} else {
						str += encodeURIComponent(`room[${index}].`) + key + '=' + encodeURIComponent(item[key]);
					}
				}
				ctrl.routerparamurlstr = str;
				console.log(str);
				console.log('************************************************************************');
			});

			/*var str = '';
			for (var key in ctrl.homebooknow.rooms) {
				if (str != '') {
					str += '&';
				}
				str += key + '=' + encodeURIComponent(ctrl.homebooknow.rooms[key]);
			}
			console.log(str);
			console.log('************************************************************************');*/

			//var string = '&'


			//var paramString = `http://holidaysforall.in/tripsolutions/rate/hotel/detail?checkInDate=${checkindate}&checkOutDate=${checkoutdate}&rooms%5B${ctrl.homebooknow.rooms}%5D.adults=2&city=${ctrl.homebooknow.location}&rooms%5B1%5D.adults=3&hotelid=${ctrl.homebooknow.hotelid}&ratePlanId=0001312515&roomTypeId=0000045636&rooms%5B0%5D.childAges=5,10&rooms%5B0%5D.childs=2`;
			var paramString = `checkInDate=${checkindate}&checkOutDate=${checkoutdate}&city=${ctrl.homebooknow.location}&`;
			console.log(paramString);
			$window.location.href = '/city/'+ ctrl.homebooknow.location.toLowerCase() + `?checkInDate=${checkindate}&checkOutDate=${checkoutdate}&city=${ctrl.homebooknow.location}&` + ctrl.routerparamurlstr;
		};

		ctrl.updateCheckInOut = function () {
			console.log(ctrl.datePicker);
		};

		ctrl.showDatePicker = function () {
			$('#dateRangePickerhide').trigger('click');
		};

		$scope.$watch('ctrl.datePicker', function (newVal) {
			console.log(newVal);
			if (newVal.startDate && newVal.endDate) {
				ctrl.homebooknow.checkin = newVal.startDate._d;
				ctrl.homebooknow.checkout = newVal.endDate._d;
			}
		}, true);

	});