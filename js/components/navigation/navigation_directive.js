angular.module('firstApp')
	.directive('navigation', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'navigationCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/navigation.html'
		};
	});