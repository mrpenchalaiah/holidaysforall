angular.module('firstApp')
	.directive('searchresults', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'searchresultsCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/searchresults.html'
		};
	});