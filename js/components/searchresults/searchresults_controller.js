angular.module('firstApp')
	.controller('searchresultsCtrl', function ($routeParams, $scope, CityhotelsService, $window, $http) {
		var ctrl = this;
		ctrl.t = {};
		ctrl.routeParams = $routeParams;
		ctrl.cityhotels = [];

		ctrl.urldata = {};

		CityhotelsService.getAll(ctrl.routeParams.cityname).then(function (cityhotels) {
			console.log('cityhotels====================');
			console.log(cityhotels);
			ctrl.cityhotels = cityhotels;

			ctrl.cityhotels.map(function (item) {
				console.log(item.vid);
				$http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/rate/hotel/detail' + $window.location.search + '&hotelid=' + item.vid,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: {}
				}).then(
					function (response) {
						console.log('responze', response);
					},
					function (error) {
						console.log('error', error);
					});
			});

		});

		$scope.$watch('ctrl.routeParams', function (newVal) {
			console.log('router params');
			console.log(newVal);

			console.log($window.location.search);

		}), true;
	})
	.filter('cut', function () {
		return function (value, wordwise, max, tail) {
			if (!value) return '';

			max = parseInt(max, 10);
			if (!max) return value;
			if (value.length <= max) return value;

			value = value.substr(0, max);
			if (wordwise) {
				var lastspace = value.lastIndexOf(' ');
				if (lastspace !== -1) {
					//Also remove . and , so its gives a cleaner result.
					if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
						lastspace = lastspace - 1;
					}
					value = value.substr(0, lastspace);
				}
			}

			return value + (tail || ' …');
		};
	});