angular.module('firstApp')
	.controller('hotelpageCtrl', function($routeParams, $scope, HotelService, HotelPricingService) {
		var ctrl = this;
		ctrl.t = {};
		ctrl.routeParams = $routeParams;
		ctrl.pricing = {};
		ctrl.hotel = {};

		HotelService.getAll(ctrl.routeParams.hotelid).then(function(hotel) {
			console.log(hotel);
			ctrl.hotel = hotel;
		});

		HotelPricingService.getAll().then(function(pricing) {
			console.log('hotelpricing=============================');
			console.log(pricing);
			ctrl.pricing = pricing;
		});

	});