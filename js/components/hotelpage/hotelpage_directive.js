angular.module('firstApp')
	.directive('hotelpage', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'hotelpageCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/hotelpage.html'
		};
	});