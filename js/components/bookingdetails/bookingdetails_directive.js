angular.module('firstApp')
	.directive('bookingdetails', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'bookingdetailsCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/bookingdetails.html'
		};
	});