angular.module('firstApp')
	.directive('bookingthankyou', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'bookingthankyouCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/bookingthankyou.html'
		};
	});