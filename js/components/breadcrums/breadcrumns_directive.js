angular.module('firstApp')
	.directive('breadcrums', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'breadcrumsCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/breadcrums.html'
		};
	});