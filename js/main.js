angular.module('firstApp', ['ngSanitize', 'ngRoute', 'daterangepicker', 'ui.select', 'angularMoment'])
	.config(function ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', {
				template: '<home></home>'
			})
			.when('/hotel', {
				template: '<hotelpage></hotelpage>',
			})
			.when('/booking-details', {
				template: '<bookingdetails></bookingdetails>',
			})
			.when('/booking-thankyou', {
				template: '<bookingthankyou></bookingthankyou>',
			})
			.when('/search-results', {
				template: '<searchresults></searchresults>',
			})
			.when('/city/:cityname', {
				template: '<searchresults></searchresults>',
			})
			.when('/city/:cityname/:hotelid', {
				template: '<hotelpage></hotelpage>',
			})
			.otherwise({
				redirectTo: '/'
			});
		$locationProvider.html5Mode(true);
	})
	.run(function ($rootScope, $location) {
		$rootScope.$on('$routeChangeStart', function (event, next, current) {
			console.log('state has beeeeeeeennnnnnnnn changedddd');
			if ($location.path() === '/login') {
				$rootScope.currentclass = 'login-layout';
			} else {
				$rootScope.currentclass = 'no-skin';
			}
			var ishomepage = ($location.path() === '/') ? true : false;
			if (ishomepage) {
				$('#mainwrapper').addClass('homemainbody');
			} else {
				$('#mainwrapper').removeClass('homemainbody');
			}
		});
	});