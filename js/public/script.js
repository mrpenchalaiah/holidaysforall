angular.module('firstApp', ['ngSanitize', 'ngRoute', 'daterangepicker', 'ui.select', 'angularMoment'])
	.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', {
				template: '<home></home>'
			})
			.when('/hotel', {
				template: '<hotelpage></hotelpage>',
			})
			.when('/booking-details', {
				template: '<bookingdetails></bookingdetails>',
			})
			.when('/booking-thankyou', {
				template: '<bookingthankyou></bookingthankyou>',
			})
			.when('/search-results', {
				template: '<searchresults></searchresults>',
			})
			.when('/city/:cityname', {
				template: '<searchresults></searchresults>',
			})
			.when('/city/:cityname/:hotelid', {
				template: '<hotelpage></hotelpage>',
			})
			.otherwise({
				redirectTo: '/'
			});
		$locationProvider.html5Mode(true);
	}])
	.run(['$rootScope', '$location', function ($rootScope, $location) {
		$rootScope.$on('$routeChangeStart', function (event, next, current) {
			console.log('state has beeeeeeeennnnnnnnn changedddd');
			if ($location.path() === '/login') {
				$rootScope.currentclass = 'login-layout';
			} else {
				$rootScope.currentclass = 'no-skin';
			}
			var ishomepage = ($location.path() === '/') ? true : false;
			if (ishomepage) {
				$('#mainwrapper').addClass('homemainbody');
			} else {
				$('#mainwrapper').removeClass('homemainbody');
			}
		});
	}]);
angular.module('firstApp')
	.controller('bookingdetailsCtrl', function() {
		var ctrl = this;
		ctrl.t = {};
	});
angular.module('firstApp')
	.directive('bookingdetails', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'bookingdetailsCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/bookingdetails.html'
		};
	});
angular.module('firstApp')
	.controller('bookingthankyouCtrl', function() {
		var ctrl = this;
		ctrl.t = {};
	});
angular.module('firstApp')
	.directive('bookingthankyou', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'bookingthankyouCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/bookingthankyou.html'
		};
	});
angular.module('firstApp')
	.controller('breadcrumsCtrl', function() {
		var ctrl = this;
		ctrl.t = {};
	});
angular.module('firstApp')
	.directive('breadcrums', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'breadcrumsCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/breadcrums.html'
		};
	});
angular.module('firstApp')
	.controller('homeCtrl', ['CityService', '$routeParams', '$window', '$scope', function (CityService, $routeParams, $window, $scope) {
		var ctrl = this;
		ctrl.t = {
			title: 'Home page',
		};
		ctrl.cities = [];
		ctrl.datePicker = {
			startDate: null,
			endDate: null
		};

		ctrl.agelist = [{
			label: '1',
			value: '1'
		},
		{
			label: '2',
			value: '2'
		},
		{
			label: '3',
			value: '3'
		},
		{
			label: '4',
			value: '4'
		},
		{
			label: '5',
			value: '5'
		},
		{
			label: '6',
			value: '6'
		},
		{
			label: '7',
			value: '7'
		},
		{
			label: '8',
			value: '8'
		},
		{
			label: '9',
			value: '9'
		},
		{
			label: '10',
			value: '10'
		},
		{
			label: '11',
			value: '11'
		},
		{
			label: '12',
			value: '12'
		},
		];

		ctrl.datePickerOptins = {
			locale: {
				applyLabel: 'Apply',
				fromLabel: 'Check In',
				///format: 'YYYY-MM-DD', //will give you 2017-01-06
				//format: 'D-MMM-YY', //will give you 6-Jan-17
				format: 'DD-MM-YYYY', //will give you 6-January-17
				toLabel: 'Check Out',
				cancelLabel: 'Cancel',
				separator: ' to '
				//customRangeLabel: 'Custom range'
			}
		};

		ctrl.homebooknow = {
			type: 'hotel',
			location: 'BANGALORE',
			checkin: new Date(),
			checkout: new Date(new Date().setDate(new Date().getDate() + 10)),
			rooms: [{
				adults: 1,
				childs: 0,
				childAges: [],
			}],
			// adults: 1,
			// child: 0,
			// childAge: [],
		};

		CityService.getAll().then(function (cities) {
			ctrl.cities = cities;
			console.log(ctrl.cities);
		});

		ctrl.updateLocation = function () {
			console.log(ctrl.location);
			ctrl.homebooknow.location = ctrl.location.ca;
		};

		ctrl.addMore = function (type, index) {
			console.log(index);
			if (type == 'rooms') {
				ctrl.homebooknow.rooms.push({
					adults: 1,
					childs: 0,
					childAges: [],
				});
			} else if (type == 'adults') {
				console.log('i came here');
				console.log(type)
				console.log(index)
				console.log(ctrl.homebooknow.rooms[index]);
				if (parseInt(ctrl.homebooknow.rooms[index][type]) > 0) {
					console.log('console.log')
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] + 1);
				}
			} else if (type == 'childs') {
				if (parseInt(ctrl.homebooknow.rooms[index][type]) >= 0) {
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] + 1);
					ctrl.homebooknow.rooms[index].childAges.push({
						age: '1'
					});
				}
			}
		};
		ctrl.removeSome = function (type, index) {
			console.log(index);
			if (type == 'rooms') {
				if (!(parseInt(ctrl.homebooknow[type]) <= 1)) {
					ctrl.homebooknow.rooms = ctrl.homebooknow.rooms.slice(0, -1);
				}
			} else if (type == 'adults') {
				if (!(parseInt(ctrl.homebooknow.rooms[index][type]) <= 1)) {
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] - 1);
				}
			} else if (type == 'childs') {
				if (!(parseInt(ctrl.homebooknow.rooms[index][type]) <= 0)) {
					ctrl.homebooknow.rooms[index][type] = parseInt(ctrl.homebooknow.rooms[index][type] - 1);
					console.log('removing part');
					console.log(ctrl.homebooknow.childAges);
					ctrl.homebooknow.rooms[index].childAges = ctrl.homebooknow.rooms[index].childAges.slice(0, -1);
				}
			}
		};

		ctrl.booknow = function () {
			console.log('************************************************************************');
			console.log(ctrl.homebooknow);
			var checkindate = moment(ctrl.homebooknow.checkin).format('DD-MM-YYYY');
			var checkoutdate = moment(ctrl.homebooknow.checkout).format('DD-MM-YYYY');


			ctrl.homebooknow.rooms.map(function (item, index) {
				console.log(item);
				var str = '';
				for (var key in item) {
					console.log(key, 'key')
					if (str != '') {
						str += '&';
					}
					if (key == 'childAges') {
						var childage = [];
						item[key].map(function (item) {
							childage.push(item.age);
						});
						console.log(childage);
						str += encodeURIComponent(`room[${index}].`) + key + '=' + childage.toString();
					} else {
						str += encodeURIComponent(`room[${index}].`) + key + '=' + encodeURIComponent(item[key]);
					}
				}
				ctrl.routerparamurlstr = str;
				console.log(str);
				console.log('************************************************************************');
			});

			/*var str = '';
			for (var key in ctrl.homebooknow.rooms) {
				if (str != '') {
					str += '&';
				}
				str += key + '=' + encodeURIComponent(ctrl.homebooknow.rooms[key]);
			}
			console.log(str);
			console.log('************************************************************************');*/

			//var string = '&'


			//var paramString = `http://holidaysforall.in/tripsolutions/rate/hotel/detail?checkInDate=${checkindate}&checkOutDate=${checkoutdate}&rooms%5B${ctrl.homebooknow.rooms}%5D.adults=2&city=${ctrl.homebooknow.location}&rooms%5B1%5D.adults=3&hotelid=${ctrl.homebooknow.hotelid}&ratePlanId=0001312515&roomTypeId=0000045636&rooms%5B0%5D.childAges=5,10&rooms%5B0%5D.childs=2`;
			var paramString = `checkInDate=${checkindate}&checkOutDate=${checkoutdate}&city=${ctrl.homebooknow.location}&`;
			console.log(paramString);
			$window.location.href = '/city/'+ ctrl.homebooknow.location.toLowerCase() + `?checkInDate=${checkindate}&checkOutDate=${checkoutdate}&city=${ctrl.homebooknow.location}&` + ctrl.routerparamurlstr;
		};

		ctrl.updateCheckInOut = function () {
			console.log(ctrl.datePicker);
		};

		ctrl.showDatePicker = function () {
			$('#dateRangePickerhide').trigger('click');
		};

		$scope.$watch('ctrl.datePicker', function (newVal) {
			console.log(newVal);
			if (newVal.startDate && newVal.endDate) {
				ctrl.homebooknow.checkin = newVal.startDate._d;
				ctrl.homebooknow.checkout = newVal.endDate._d;
			}
		}, true);

	}]);

angular.module('firstApp')
	.directive('home', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'homeCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/home.html'
		};
	});
angular.module('firstApp')
	.controller('hotelpageCtrl', ['$routeParams', '$scope', 'HotelService', 'HotelPricingService', function($routeParams, $scope, HotelService, HotelPricingService) {
		var ctrl = this;
		ctrl.t = {};
		ctrl.routeParams = $routeParams;
		ctrl.pricing = {};
		ctrl.hotel = {};

		HotelService.getAll(ctrl.routeParams.hotelid).then(function(hotel) {
			console.log(hotel);
			ctrl.hotel = hotel;
		});

		HotelPricingService.getAll().then(function(pricing) {
			console.log('hotelpricing=============================');
			console.log(pricing);
			ctrl.pricing = pricing;
		});

	}]);
angular.module('firstApp')
	.directive('hotelpage', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'hotelpageCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/hotelpage.html'
		};
	});
angular.module('firstApp')
	.controller('navigationCtrl', function() {
		var ctrl = this;
		ctrl.t = {};
	});
angular.module('firstApp')
	.directive('navigation', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'navigationCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/navigation.html'
		};
	});
angular.module('firstApp')
	.controller('searchresultsCtrl', ['$routeParams', '$scope', 'CityhotelsService', '$window', '$http', function ($routeParams, $scope, CityhotelsService, $window, $http) {
		var ctrl = this;
		ctrl.t = {};
		ctrl.routeParams = $routeParams;
		ctrl.cityhotels = [];

		ctrl.urldata = {};

		CityhotelsService.getAll(ctrl.routeParams.cityname).then(function (cityhotels) {
			console.log('cityhotels====================');
			console.log(cityhotels);
			ctrl.cityhotels = cityhotels;

			ctrl.cityhotels.map(function (item) {
				console.log(item.vid);
				$http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/rate/hotel/detail' + $window.location.search + '&hotelid=' + item.vid,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: {}
				}).then(
					function (response) {
						console.log('responze', response);
					},
					function (error) {
						console.log('error', error);
					});
			});

		});

		$scope.$watch('ctrl.routeParams', function (newVal) {
			console.log('router params');
			console.log(newVal);

			console.log($window.location.search);

		}), true;
	}])
	.filter('cut', function () {
		return function (value, wordwise, max, tail) {
			if (!value) return '';

			max = parseInt(max, 10);
			if (!max) return value;
			if (value.length <= max) return value;

			value = value.substr(0, max);
			if (wordwise) {
				var lastspace = value.lastIndexOf(' ');
				if (lastspace !== -1) {
					//Also remove . and , so its gives a cleaner result.
					if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
						lastspace = lastspace - 1;
					}
					value = value.substr(0, lastspace);
				}
			}

			return value + (tail || ' …');
		};
	});
angular.module('firstApp')
	.directive('searchresults', function() {
		return {
			restrict: 'EA',
			scope: {},
			controller: 'searchresultsCtrl',
			controllerAs: 'ctrl',
			bindToController: {},
			templateUrl: './templates/searchresults.html'
		};
	});
angular.module('firstApp')
	.factory('Hotel', function () {
		return function Hotel(hotel) {
			angular.extend(this, hotel);
			console.log(hotel);
		};
	})
	.factory('CityhotelsService', ['$q', '$http', 'Hotel', function ($q, $http, Hotel) {

		var cityhotels = [];
		var loadPromise = undefined;
		var loadAll = function (cityname) {
			if (cityhotels.loadPromise > 0) {
				return $q.when(cityhotels);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/content/city?cityName='+ cityname.toUpperCase(),
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
						if(data.data.data) {
							cityhotels = data.data.data.map(function (hotel) {
								return new Hotel(hotel);
							});
						}
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function (cityname) {
				return loadAll(cityname).then(function () {
					return cityhotels;
				});
			}
		};

	}]);
angular.module('firstApp')
	.factory('City', function () {
		return function City(city) {
			angular.extend(this, city);
		};
	})
	.factory('CityService', ['$q', '$http', 'City', function ($q, $http, City) {

		var cities = [];
		var loadPromise = undefined;
		var loadAll = function () {
			if (cities.loadPromise > 0) {
				return $q.when(cities);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/cities',
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
						cities = data.data.data.map(function (city) {
							return new City(city);
						});
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function () {
				return loadAll().then(function () {
					return cities;
				});
			}
		};

	}]);
angular.module('firstApp')
	.factory('HotelPricing', function () {
		return function HotelPricing(hotelprice) {
            console.log('i came here');
            console.log(hotelprice);
			angular.extend(this, hotelprice);
		};
	})
	.factory('HotelPricingService', ['$q', '$http', 'HotelPricing', function ($q, $http, HotelPricing) {

		var hotelprice = [];
		var loadPromise = undefined;
		var loadAll = function () {
			if (hotelprice.loadPromise > 0) {
				return $q.when(hotelprice);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/rate/hotel/detail?checkInDate=05-09-2018&checkOutDate=06-09-2018&rooms%5B0%5D.adults=2&city=Bangalore&rooms%5B1%5D.adults=3&hotelid=00012864&ratePlanId=0001312515&roomTypeId=0000045636&rooms%5B0%5D.childAges=5,10&rooms%5B0%5D.childs=2',
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
                        console.log(data.data.data);
						hotelprice = new HotelPricing(data.data.data);
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function () {
				return loadAll().then(function () {
					return hotelprice;
				});
			}
		};

	}]);
angular.module('firstApp')
	.factory('Hotel', function () {
		return function Hotel(hotel) {
			angular.extend(this, hotel);
			console.log(hotel);
		};
	})
	.factory('HotelService', ['$q', '$http', 'Hotel', function ($q, $http, Hotel) {

		var hotel = [];
		var loadPromise = undefined;
		var loadAll = function (hotelid) {
			if (hotel.loadPromise > 0) {
				return $q.when(hotel);
			}
			if (_.isUndefined(loadPromise)) {
				loadPromise = $http({
					method: 'GET',
					url: 'http://holidaysforall.in/tripsolutions/content/hotel?hotelid='+ hotelid,
					data: {}
				}).then(function (data) {
					loadPromise = undefined;
					console.log(data);
					if(data.status === 200) {
						hotel = new Hotel(data.data.data);
					}
				});
			}
			return loadPromise;
		};

		return {
			getAll: function (hotelid) {
				return loadAll(hotelid).then(function () {
					return hotel;
				});
			}
		};

	}]);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiLCJib29raW5nZGV0YWlscy9ib29raW5nZGV0YWlsc19jb250cm9sbGVyLmpzIiwiYm9va2luZ2RldGFpbHMvYm9va2luZ2RldGFpbHNfZGlyZWN0aXZlLmpzIiwiYm9va2luZ3RoYW5reW91L2Jvb2tpbmd0aGFua3lvdV9jb250cm9sbGVyLmpzIiwiYm9va2luZ3RoYW5reW91L2Jvb2tpbmd0aGFua3lvdV9kaXJlY3RpdmUuanMiLCJicmVhZGNydW1zL2JyZWFkY3J1bW5zX2NvbnRyb2xsZXIuanMiLCJicmVhZGNydW1zL2JyZWFkY3J1bW5zX2RpcmVjdGl2ZS5qcyIsImhvbWUvaG9tZV9jb250cm9sbGVyLmpzIiwiaG9tZS9ob21lX2RpcmVjdGl2ZS5qcyIsImhvdGVscGFnZS9ob3RlbHBhZ2VfY29udHJvbGxlci5qcyIsImhvdGVscGFnZS9ob3RlbHBhZ2VfZGlyZWN0aXZlLmpzIiwibmF2aWdhdGlvbi9uYXZpZ2F0aW9uX2NvbnRyb2xsZXIuanMiLCJuYXZpZ2F0aW9uL25hdmlnYXRpb25fZGlyZWN0aXZlLmpzIiwic2VhcmNocmVzdWx0cy9zZWFyY2hyZXN1bHRzX2NvbnRyb2xsZXIuanMiLCJzZWFyY2hyZXN1bHRzL3NlYXJjaHJlc3VsdHNfZGlyZWN0aXZlLmpzIiwiY2l0eWhvdGVsc19zZXJ2aWNlLmpzIiwiY2l0eV9zZXJ2aWNlLmpzIiwiaG90ZWxwcmljaW5nX3NlcnZpY2UuanMiLCJob3RlbF9zZXJ2aWNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFFBQVEsT0FBTyxZQUFZLENBQUMsY0FBYyxXQUFXLG1CQUFtQixhQUFhO0VBQ25GLCtDQUFPLFVBQVUsZ0JBQWdCLG1CQUFtQjtFQUNwRDtJQUNFLEtBQUssS0FBSztJQUNWLFVBQVU7O0lBRVYsS0FBSyxVQUFVO0lBQ2YsVUFBVTs7SUFFVixLQUFLLG9CQUFvQjtJQUN6QixVQUFVOztJQUVWLEtBQUsscUJBQXFCO0lBQzFCLFVBQVU7O0lBRVYsS0FBSyxtQkFBbUI7SUFDeEIsVUFBVTs7SUFFVixLQUFLLG1CQUFtQjtJQUN4QixVQUFVOztJQUVWLEtBQUssNEJBQTRCO0lBQ2pDLFVBQVU7O0lBRVYsVUFBVTtJQUNWLFlBQVk7O0VBRWQsa0JBQWtCLFVBQVU7O0VBRTVCLGdDQUFJLFVBQVUsWUFBWSxXQUFXO0VBQ3JDLFdBQVcsSUFBSSxxQkFBcUIsVUFBVSxPQUFPLE1BQU0sU0FBUztHQUNuRSxRQUFRLElBQUk7R0FDWixJQUFJLFVBQVUsV0FBVyxVQUFVO0lBQ2xDLFdBQVcsZUFBZTtVQUNwQjtJQUNOLFdBQVcsZUFBZTs7R0FFM0IsSUFBSSxhQUFhLENBQUMsVUFBVSxXQUFXLE9BQU8sT0FBTztHQUNyRCxJQUFJLFlBQVk7SUFDZixFQUFFLGdCQUFnQixTQUFTO1VBQ3JCO0lBQ04sRUFBRSxnQkFBZ0IsWUFBWTs7O0tBRzlCO0FDNUNKLFFBQVEsT0FBTztFQUNiLFdBQVcsc0JBQXNCLFdBQVc7RUFDNUMsSUFBSSxPQUFPO0VBQ1gsS0FBSyxJQUFJO0lBQ1A7QUNKSixRQUFRLE9BQU87RUFDYixVQUFVLGtCQUFrQixXQUFXO0VBQ3ZDLE9BQU87R0FDTixVQUFVO0dBQ1YsT0FBTztHQUNQLFlBQVk7R0FDWixjQUFjO0dBQ2Qsa0JBQWtCO0dBQ2xCLGFBQWE7O0lBRVo7QUNWSixRQUFRLE9BQU87RUFDYixXQUFXLHVCQUF1QixXQUFXO0VBQzdDLElBQUksT0FBTztFQUNYLEtBQUssSUFBSTtJQUNQO0FDSkosUUFBUSxPQUFPO0VBQ2IsVUFBVSxtQkFBbUIsV0FBVztFQUN4QyxPQUFPO0dBQ04sVUFBVTtHQUNWLE9BQU87R0FDUCxZQUFZO0dBQ1osY0FBYztHQUNkLGtCQUFrQjtHQUNsQixhQUFhOztJQUVaO0FDVkosUUFBUSxPQUFPO0VBQ2IsV0FBVyxrQkFBa0IsV0FBVztFQUN4QyxJQUFJLE9BQU87RUFDWCxLQUFLLElBQUk7SUFDUDtBQ0pKLFFBQVEsT0FBTztFQUNiLFVBQVUsY0FBYyxXQUFXO0VBQ25DLE9BQU87R0FDTixVQUFVO0dBQ1YsT0FBTztHQUNQLFlBQVk7R0FDWixjQUFjO0dBQ2Qsa0JBQWtCO0dBQ2xCLGFBQWE7O0lBRVo7QUNWSixRQUFRLE9BQU87RUFDYixXQUFXLGlFQUFZLFVBQVUsYUFBYSxjQUFjLFNBQVMsUUFBUTtFQUM3RSxJQUFJLE9BQU87RUFDWCxLQUFLLElBQUk7R0FDUixPQUFPOztFQUVSLEtBQUssU0FBUztFQUNkLEtBQUssYUFBYTtHQUNqQixXQUFXO0dBQ1gsU0FBUzs7O0VBR1YsS0FBSyxVQUFVLENBQUM7R0FDZixPQUFPO0dBQ1AsT0FBTzs7RUFFUjtHQUNDLE9BQU87R0FDUCxPQUFPOztFQUVSO0dBQ0MsT0FBTztHQUNQLE9BQU87O0VBRVI7R0FDQyxPQUFPO0dBQ1AsT0FBTzs7RUFFUjtHQUNDLE9BQU87R0FDUCxPQUFPOztFQUVSO0dBQ0MsT0FBTztHQUNQLE9BQU87O0VBRVI7R0FDQyxPQUFPO0dBQ1AsT0FBTzs7RUFFUjtHQUNDLE9BQU87R0FDUCxPQUFPOztFQUVSO0dBQ0MsT0FBTztHQUNQLE9BQU87O0VBRVI7R0FDQyxPQUFPO0dBQ1AsT0FBTzs7RUFFUjtHQUNDLE9BQU87R0FDUCxPQUFPOztFQUVSO0dBQ0MsT0FBTztHQUNQLE9BQU87Ozs7RUFJUixLQUFLLG1CQUFtQjtHQUN2QixRQUFRO0lBQ1AsWUFBWTtJQUNaLFdBQVc7OztJQUdYLFFBQVE7SUFDUixTQUFTO0lBQ1QsYUFBYTtJQUNiLFdBQVc7Ozs7O0VBS2IsS0FBSyxjQUFjO0dBQ2xCLE1BQU07R0FDTixVQUFVO0dBQ1YsU0FBUyxJQUFJO0dBQ2IsVUFBVSxJQUFJLEtBQUssSUFBSSxPQUFPLFFBQVEsSUFBSSxPQUFPLFlBQVk7R0FDN0QsT0FBTyxDQUFDO0lBQ1AsUUFBUTtJQUNSLFFBQVE7SUFDUixXQUFXOzs7Ozs7O0VBT2IsWUFBWSxTQUFTLEtBQUssVUFBVSxRQUFRO0dBQzNDLEtBQUssU0FBUztHQUNkLFFBQVEsSUFBSSxLQUFLOzs7RUFHbEIsS0FBSyxpQkFBaUIsWUFBWTtHQUNqQyxRQUFRLElBQUksS0FBSztHQUNqQixLQUFLLFlBQVksV0FBVyxLQUFLLFNBQVM7OztFQUczQyxLQUFLLFVBQVUsVUFBVSxNQUFNLE9BQU87R0FDckMsUUFBUSxJQUFJO0dBQ1osSUFBSSxRQUFRLFNBQVM7SUFDcEIsS0FBSyxZQUFZLE1BQU0sS0FBSztLQUMzQixRQUFRO0tBQ1IsUUFBUTtLQUNSLFdBQVc7O1VBRU4sSUFBSSxRQUFRLFVBQVU7SUFDNUIsUUFBUSxJQUFJO0lBQ1osUUFBUSxJQUFJO0lBQ1osUUFBUSxJQUFJO0lBQ1osUUFBUSxJQUFJLEtBQUssWUFBWSxNQUFNO0lBQ25DLElBQUksU0FBUyxLQUFLLFlBQVksTUFBTSxPQUFPLFNBQVMsR0FBRztLQUN0RCxRQUFRLElBQUk7S0FDWixLQUFLLFlBQVksTUFBTSxPQUFPLFFBQVEsU0FBUyxLQUFLLFlBQVksTUFBTSxPQUFPLFFBQVE7O1VBRWhGLElBQUksUUFBUSxVQUFVO0lBQzVCLElBQUksU0FBUyxLQUFLLFlBQVksTUFBTSxPQUFPLFVBQVUsR0FBRztLQUN2RCxLQUFLLFlBQVksTUFBTSxPQUFPLFFBQVEsU0FBUyxLQUFLLFlBQVksTUFBTSxPQUFPLFFBQVE7S0FDckYsS0FBSyxZQUFZLE1BQU0sT0FBTyxVQUFVLEtBQUs7TUFDNUMsS0FBSzs7Ozs7RUFLVCxLQUFLLGFBQWEsVUFBVSxNQUFNLE9BQU87R0FDeEMsUUFBUSxJQUFJO0dBQ1osSUFBSSxRQUFRLFNBQVM7SUFDcEIsSUFBSSxFQUFFLFNBQVMsS0FBSyxZQUFZLFVBQVUsSUFBSTtLQUM3QyxLQUFLLFlBQVksUUFBUSxLQUFLLFlBQVksTUFBTSxNQUFNLEdBQUcsQ0FBQzs7VUFFckQsSUFBSSxRQUFRLFVBQVU7SUFDNUIsSUFBSSxFQUFFLFNBQVMsS0FBSyxZQUFZLE1BQU0sT0FBTyxVQUFVLElBQUk7S0FDMUQsS0FBSyxZQUFZLE1BQU0sT0FBTyxRQUFRLFNBQVMsS0FBSyxZQUFZLE1BQU0sT0FBTyxRQUFROztVQUVoRixJQUFJLFFBQVEsVUFBVTtJQUM1QixJQUFJLEVBQUUsU0FBUyxLQUFLLFlBQVksTUFBTSxPQUFPLFVBQVUsSUFBSTtLQUMxRCxLQUFLLFlBQVksTUFBTSxPQUFPLFFBQVEsU0FBUyxLQUFLLFlBQVksTUFBTSxPQUFPLFFBQVE7S0FDckYsUUFBUSxJQUFJO0tBQ1osUUFBUSxJQUFJLEtBQUssWUFBWTtLQUM3QixLQUFLLFlBQVksTUFBTSxPQUFPLFlBQVksS0FBSyxZQUFZLE1BQU0sT0FBTyxVQUFVLE1BQU0sR0FBRyxDQUFDOzs7OztFQUsvRixLQUFLLFVBQVUsWUFBWTtHQUMxQixRQUFRLElBQUk7R0FDWixRQUFRLElBQUksS0FBSztHQUNqQixJQUFJLGNBQWMsT0FBTyxLQUFLLFlBQVksU0FBUyxPQUFPO0dBQzFELElBQUksZUFBZSxPQUFPLEtBQUssWUFBWSxVQUFVLE9BQU87OztHQUc1RCxLQUFLLFlBQVksTUFBTSxJQUFJLFVBQVUsTUFBTSxPQUFPO0lBQ2pELFFBQVEsSUFBSTtJQUNaLElBQUksTUFBTTtJQUNWLEtBQUssSUFBSSxPQUFPLE1BQU07S0FDckIsUUFBUSxJQUFJLEtBQUs7S0FDakIsSUFBSSxPQUFPLElBQUk7TUFDZCxPQUFPOztLQUVSLElBQUksT0FBTyxhQUFhO01BQ3ZCLElBQUksV0FBVztNQUNmLEtBQUssS0FBSyxJQUFJLFVBQVUsTUFBTTtPQUM3QixTQUFTLEtBQUssS0FBSzs7TUFFcEIsUUFBUSxJQUFJO01BQ1osT0FBTyxtQkFBbUIscUJBQXFCLE1BQU0sTUFBTSxTQUFTO1lBQzlEO01BQ04sT0FBTyxtQkFBbUIscUJBQXFCLE1BQU0sTUFBTSxtQkFBbUIsS0FBSzs7O0lBR3JGLEtBQUssb0JBQW9CO0lBQ3pCLFFBQVEsSUFBSTtJQUNaLFFBQVEsSUFBSTs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FpQmIsSUFBSSxjQUFjO0dBQ2xCLFFBQVEsSUFBSTtHQUNaLFFBQVEsU0FBUyxPQUFPLFVBQVUsS0FBSyxZQUFZLFNBQVMsZ0JBQWdCLGdHQUFnRyxLQUFLOzs7RUFHbEwsS0FBSyxtQkFBbUIsWUFBWTtHQUNuQyxRQUFRLElBQUksS0FBSzs7O0VBR2xCLEtBQUssaUJBQWlCLFlBQVk7R0FDakMsRUFBRSx3QkFBd0IsUUFBUTs7O0VBR25DLE9BQU8sT0FBTyxtQkFBbUIsVUFBVSxRQUFRO0dBQ2xELFFBQVEsSUFBSTtHQUNaLElBQUksT0FBTyxhQUFhLE9BQU8sU0FBUztJQUN2QyxLQUFLLFlBQVksVUFBVSxPQUFPLFVBQVU7SUFDNUMsS0FBSyxZQUFZLFdBQVcsT0FBTyxRQUFROztLQUUxQzs7S0FFRDtBQ3JOSjtBQUNBLFFBQVEsT0FBTztFQUNiLFVBQVUsUUFBUSxXQUFXO0VBQzdCLE9BQU87R0FDTixVQUFVO0dBQ1YsT0FBTztHQUNQLFlBQVk7R0FDWixjQUFjO0dBQ2Qsa0JBQWtCO0dBQ2xCLGFBQWE7O0lBRVo7QUNYSixRQUFRLE9BQU87RUFDYixXQUFXLG1GQUFpQixTQUFTLGNBQWMsUUFBUSxjQUFjLHFCQUFxQjtFQUM5RixJQUFJLE9BQU87RUFDWCxLQUFLLElBQUk7RUFDVCxLQUFLLGNBQWM7RUFDbkIsS0FBSyxVQUFVO0VBQ2YsS0FBSyxRQUFROztFQUViLGFBQWEsT0FBTyxLQUFLLFlBQVksU0FBUyxLQUFLLFNBQVMsT0FBTztHQUNsRSxRQUFRLElBQUk7R0FDWixLQUFLLFFBQVE7OztFQUdkLG9CQUFvQixTQUFTLEtBQUssU0FBUyxTQUFTO0dBQ25ELFFBQVEsSUFBSTtHQUNaLFFBQVEsSUFBSTtHQUNaLEtBQUssVUFBVTs7O0tBR2Q7QUNuQkosUUFBUSxPQUFPO0VBQ2IsVUFBVSxhQUFhLFdBQVc7RUFDbEMsT0FBTztHQUNOLFVBQVU7R0FDVixPQUFPO0dBQ1AsWUFBWTtHQUNaLGNBQWM7R0FDZCxrQkFBa0I7R0FDbEIsYUFBYTs7SUFFWjtBQ1ZKLFFBQVEsT0FBTztFQUNiLFdBQVcsa0JBQWtCLFdBQVc7RUFDeEMsSUFBSSxPQUFPO0VBQ1gsS0FBSyxJQUFJO0lBQ1A7QUNKSixRQUFRLE9BQU87RUFDYixVQUFVLGNBQWMsV0FBVztFQUNuQyxPQUFPO0dBQ04sVUFBVTtHQUNWLE9BQU87R0FDUCxZQUFZO0dBQ1osY0FBYztHQUNkLGtCQUFrQjtHQUNsQixhQUFhOztJQUVaO0FDVkosUUFBUSxPQUFPO0VBQ2IsV0FBVyx5RkFBcUIsVUFBVSxjQUFjLFFBQVEsbUJBQW1CLFNBQVMsT0FBTztFQUNuRyxJQUFJLE9BQU87RUFDWCxLQUFLLElBQUk7RUFDVCxLQUFLLGNBQWM7RUFDbkIsS0FBSyxhQUFhOztFQUVsQixLQUFLLFVBQVU7O0VBRWYsa0JBQWtCLE9BQU8sS0FBSyxZQUFZLFVBQVUsS0FBSyxVQUFVLFlBQVk7R0FDOUUsUUFBUSxJQUFJO0dBQ1osUUFBUSxJQUFJO0dBQ1osS0FBSyxhQUFhOztHQUVsQixLQUFLLFdBQVcsSUFBSSxVQUFVLE1BQU07SUFDbkMsUUFBUSxJQUFJLEtBQUs7SUFDakIsTUFBTTtLQUNMLFFBQVE7S0FDUixLQUFLLDZEQUE2RCxRQUFRLFNBQVMsU0FBUyxjQUFjLEtBQUs7S0FDL0csU0FBUztNQUNSLGdCQUFnQjs7S0FFakIsTUFBTTtPQUNKO0tBQ0YsVUFBVSxVQUFVO01BQ25CLFFBQVEsSUFBSSxZQUFZOztLQUV6QixVQUFVLE9BQU87TUFDaEIsUUFBUSxJQUFJLFNBQVM7Ozs7OztFQU16QixPQUFPLE9BQU8sb0JBQW9CLFVBQVUsUUFBUTtHQUNuRCxRQUFRLElBQUk7R0FDWixRQUFRLElBQUk7O0dBRVosUUFBUSxJQUFJLFFBQVEsU0FBUzs7TUFFMUI7O0VBRUosT0FBTyxPQUFPLFlBQVk7RUFDMUIsT0FBTyxVQUFVLE9BQU8sVUFBVSxLQUFLLE1BQU07R0FDNUMsSUFBSSxDQUFDLE9BQU8sT0FBTzs7R0FFbkIsTUFBTSxTQUFTLEtBQUs7R0FDcEIsSUFBSSxDQUFDLEtBQUssT0FBTztHQUNqQixJQUFJLE1BQU0sVUFBVSxLQUFLLE9BQU87O0dBRWhDLFFBQVEsTUFBTSxPQUFPLEdBQUc7R0FDeEIsSUFBSSxVQUFVO0lBQ2IsSUFBSSxZQUFZLE1BQU0sWUFBWTtJQUNsQyxJQUFJLGNBQWMsQ0FBQyxHQUFHOztLQUVyQixJQUFJLE1BQU0sT0FBTyxZQUFZLE9BQU8sT0FBTyxNQUFNLE9BQU8sWUFBWSxPQUFPLEtBQUs7TUFDL0UsWUFBWSxZQUFZOztLQUV6QixRQUFRLE1BQU0sT0FBTyxHQUFHOzs7O0dBSTFCLE9BQU8sU0FBUyxRQUFROztJQUV2QjtBQ2hFSixRQUFRLE9BQU87RUFDYixVQUFVLGlCQUFpQixXQUFXO0VBQ3RDLE9BQU87R0FDTixVQUFVO0dBQ1YsT0FBTztHQUNQLFlBQVk7R0FDWixjQUFjO0dBQ2Qsa0JBQWtCO0dBQ2xCLGFBQWE7O0lBRVo7QUNWSixRQUFRLE9BQU87RUFDYixRQUFRLFNBQVMsWUFBWTtFQUM3QixPQUFPLFNBQVMsTUFBTSxPQUFPO0dBQzVCLFFBQVEsT0FBTyxNQUFNO0dBQ3JCLFFBQVEsSUFBSTs7O0VBR2IsUUFBUSw4Q0FBcUIsVUFBVSxJQUFJLE9BQU8sT0FBTzs7RUFFekQsSUFBSSxhQUFhO0VBQ2pCLElBQUksY0FBYztFQUNsQixJQUFJLFVBQVUsVUFBVSxVQUFVO0dBQ2pDLElBQUksV0FBVyxjQUFjLEdBQUc7SUFDL0IsT0FBTyxHQUFHLEtBQUs7O0dBRWhCLElBQUksRUFBRSxZQUFZLGNBQWM7SUFDL0IsY0FBYyxNQUFNO0tBQ25CLFFBQVE7S0FDUixLQUFLLGlFQUFpRSxTQUFTO0tBQy9FLE1BQU07T0FDSixLQUFLLFVBQVUsTUFBTTtLQUN2QixjQUFjO0tBQ2QsUUFBUSxJQUFJO0tBQ1osR0FBRyxLQUFLLFdBQVcsS0FBSztNQUN2QixHQUFHLEtBQUssS0FBSyxNQUFNO09BQ2xCLGFBQWEsS0FBSyxLQUFLLEtBQUssSUFBSSxVQUFVLE9BQU87UUFDaEQsT0FBTyxJQUFJLE1BQU07Ozs7OztHQU10QixPQUFPOzs7RUFHUixPQUFPO0dBQ04sUUFBUSxVQUFVLFVBQVU7SUFDM0IsT0FBTyxRQUFRLFVBQVUsS0FBSyxZQUFZO0tBQ3pDLE9BQU87Ozs7O0tBS1I7QUMzQ0osUUFBUSxPQUFPO0VBQ2IsUUFBUSxRQUFRLFlBQVk7RUFDNUIsT0FBTyxTQUFTLEtBQUssTUFBTTtHQUMxQixRQUFRLE9BQU8sTUFBTTs7O0VBR3RCLFFBQVEsdUNBQWUsVUFBVSxJQUFJLE9BQU8sTUFBTTs7RUFFbEQsSUFBSSxTQUFTO0VBQ2IsSUFBSSxjQUFjO0VBQ2xCLElBQUksVUFBVSxZQUFZO0dBQ3pCLElBQUksT0FBTyxjQUFjLEdBQUc7SUFDM0IsT0FBTyxHQUFHLEtBQUs7O0dBRWhCLElBQUksRUFBRSxZQUFZLGNBQWM7SUFDL0IsY0FBYyxNQUFNO0tBQ25CLFFBQVE7S0FDUixLQUFLO0tBQ0wsTUFBTTtPQUNKLEtBQUssVUFBVSxNQUFNO0tBQ3ZCLGNBQWM7S0FDZCxRQUFRLElBQUk7S0FDWixHQUFHLEtBQUssV0FBVyxLQUFLO01BQ3ZCLFNBQVMsS0FBSyxLQUFLLEtBQUssSUFBSSxVQUFVLE1BQU07T0FDM0MsT0FBTyxJQUFJLEtBQUs7Ozs7O0dBS3BCLE9BQU87OztFQUdSLE9BQU87R0FDTixRQUFRLFlBQVk7SUFDbkIsT0FBTyxVQUFVLEtBQUssWUFBWTtLQUNqQyxPQUFPOzs7OztLQUtSO0FDeENKLFFBQVEsT0FBTztFQUNiLFFBQVEsZ0JBQWdCLFlBQVk7RUFDcEMsT0FBTyxTQUFTLGFBQWEsWUFBWTtZQUMvQixRQUFRLElBQUk7WUFDWixRQUFRLElBQUk7R0FDckIsUUFBUSxPQUFPLE1BQU07OztFQUd0QixRQUFRLHVEQUF1QixVQUFVLElBQUksT0FBTyxjQUFjOztFQUVsRSxJQUFJLGFBQWE7RUFDakIsSUFBSSxjQUFjO0VBQ2xCLElBQUksVUFBVSxZQUFZO0dBQ3pCLElBQUksV0FBVyxjQUFjLEdBQUc7SUFDL0IsT0FBTyxHQUFHLEtBQUs7O0dBRWhCLElBQUksRUFBRSxZQUFZLGNBQWM7SUFDL0IsY0FBYyxNQUFNO0tBQ25CLFFBQVE7S0FDUixLQUFLO0tBQ0wsTUFBTTtPQUNKLEtBQUssVUFBVSxNQUFNO0tBQ3ZCLGNBQWM7S0FDZCxRQUFRLElBQUk7S0FDWixHQUFHLEtBQUssV0FBVyxLQUFLO3dCQUNMLFFBQVEsSUFBSSxLQUFLLEtBQUs7TUFDeEMsYUFBYSxJQUFJLGFBQWEsS0FBSyxLQUFLOzs7O0dBSTNDLE9BQU87OztFQUdSLE9BQU87R0FDTixRQUFRLFlBQVk7SUFDbkIsT0FBTyxVQUFVLEtBQUssWUFBWTtLQUNqQyxPQUFPOzs7OztLQUtSO0FDekNKLFFBQVEsT0FBTztFQUNiLFFBQVEsU0FBUyxZQUFZO0VBQzdCLE9BQU8sU0FBUyxNQUFNLE9BQU87R0FDNUIsUUFBUSxPQUFPLE1BQU07R0FDckIsUUFBUSxJQUFJOzs7RUFHYixRQUFRLHlDQUFnQixVQUFVLElBQUksT0FBTyxPQUFPOztFQUVwRCxJQUFJLFFBQVE7RUFDWixJQUFJLGNBQWM7RUFDbEIsSUFBSSxVQUFVLFVBQVUsU0FBUztHQUNoQyxJQUFJLE1BQU0sY0FBYyxHQUFHO0lBQzFCLE9BQU8sR0FBRyxLQUFLOztHQUVoQixJQUFJLEVBQUUsWUFBWSxjQUFjO0lBQy9CLGNBQWMsTUFBTTtLQUNuQixRQUFRO0tBQ1IsS0FBSyxpRUFBaUU7S0FDdEUsTUFBTTtPQUNKLEtBQUssVUFBVSxNQUFNO0tBQ3ZCLGNBQWM7S0FDZCxRQUFRLElBQUk7S0FDWixHQUFHLEtBQUssV0FBVyxLQUFLO01BQ3ZCLFFBQVEsSUFBSSxNQUFNLEtBQUssS0FBSzs7OztHQUkvQixPQUFPOzs7RUFHUixPQUFPO0dBQ04sUUFBUSxVQUFVLFNBQVM7SUFDMUIsT0FBTyxRQUFRLFNBQVMsS0FBSyxZQUFZO0tBQ3hDLE9BQU87Ozs7O0tBS1IiLCJmaWxlIjoic2NyaXB0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJywgWyduZ1Nhbml0aXplJywgJ25nUm91dGUnLCAnZGF0ZXJhbmdlcGlja2VyJywgJ3VpLnNlbGVjdCcsICdhbmd1bGFyTW9tZW50J10pXHJcblx0LmNvbmZpZyhmdW5jdGlvbiAoJHJvdXRlUHJvdmlkZXIsICRsb2NhdGlvblByb3ZpZGVyKSB7XHJcblx0XHQkcm91dGVQcm92aWRlclxyXG5cdFx0XHQud2hlbignLycsIHtcclxuXHRcdFx0XHR0ZW1wbGF0ZTogJzxob21lPjwvaG9tZT4nXHJcblx0XHRcdH0pXHJcblx0XHRcdC53aGVuKCcvaG90ZWwnLCB7XHJcblx0XHRcdFx0dGVtcGxhdGU6ICc8aG90ZWxwYWdlPjwvaG90ZWxwYWdlPicsXHJcblx0XHRcdH0pXHJcblx0XHRcdC53aGVuKCcvYm9va2luZy1kZXRhaWxzJywge1xyXG5cdFx0XHRcdHRlbXBsYXRlOiAnPGJvb2tpbmdkZXRhaWxzPjwvYm9va2luZ2RldGFpbHM+JyxcclxuXHRcdFx0fSlcclxuXHRcdFx0LndoZW4oJy9ib29raW5nLXRoYW5reW91Jywge1xyXG5cdFx0XHRcdHRlbXBsYXRlOiAnPGJvb2tpbmd0aGFua3lvdT48L2Jvb2tpbmd0aGFua3lvdT4nLFxyXG5cdFx0XHR9KVxyXG5cdFx0XHQud2hlbignL3NlYXJjaC1yZXN1bHRzJywge1xyXG5cdFx0XHRcdHRlbXBsYXRlOiAnPHNlYXJjaHJlc3VsdHM+PC9zZWFyY2hyZXN1bHRzPicsXHJcblx0XHRcdH0pXHJcblx0XHRcdC53aGVuKCcvY2l0eS86Y2l0eW5hbWUnLCB7XHJcblx0XHRcdFx0dGVtcGxhdGU6ICc8c2VhcmNocmVzdWx0cz48L3NlYXJjaHJlc3VsdHM+JyxcclxuXHRcdFx0fSlcclxuXHRcdFx0LndoZW4oJy9jaXR5LzpjaXR5bmFtZS86aG90ZWxpZCcsIHtcclxuXHRcdFx0XHR0ZW1wbGF0ZTogJzxob3RlbHBhZ2U+PC9ob3RlbHBhZ2U+JyxcclxuXHRcdFx0fSlcclxuXHRcdFx0Lm90aGVyd2lzZSh7XHJcblx0XHRcdFx0cmVkaXJlY3RUbzogJy8nXHJcblx0XHRcdH0pO1xyXG5cdFx0JGxvY2F0aW9uUHJvdmlkZXIuaHRtbDVNb2RlKHRydWUpO1xyXG5cdH0pXHJcblx0LnJ1bihmdW5jdGlvbiAoJHJvb3RTY29wZSwgJGxvY2F0aW9uKSB7XHJcblx0XHQkcm9vdFNjb3BlLiRvbignJHJvdXRlQ2hhbmdlU3RhcnQnLCBmdW5jdGlvbiAoZXZlbnQsIG5leHQsIGN1cnJlbnQpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coJ3N0YXRlIGhhcyBiZWVlZWVlZWVubm5ubm5ubm4gY2hhbmdlZGRkZCcpO1xyXG5cdFx0XHRpZiAoJGxvY2F0aW9uLnBhdGgoKSA9PT0gJy9sb2dpbicpIHtcclxuXHRcdFx0XHQkcm9vdFNjb3BlLmN1cnJlbnRjbGFzcyA9ICdsb2dpbi1sYXlvdXQnO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCRyb290U2NvcGUuY3VycmVudGNsYXNzID0gJ25vLXNraW4nO1xyXG5cdFx0XHR9XHJcblx0XHRcdHZhciBpc2hvbWVwYWdlID0gKCRsb2NhdGlvbi5wYXRoKCkgPT09ICcvJykgPyB0cnVlIDogZmFsc2U7XHJcblx0XHRcdGlmIChpc2hvbWVwYWdlKSB7XHJcblx0XHRcdFx0JCgnI21haW53cmFwcGVyJykuYWRkQ2xhc3MoJ2hvbWVtYWluYm9keScpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCQoJyNtYWlud3JhcHBlcicpLnJlbW92ZUNsYXNzKCdob21lbWFpbmJvZHknKTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fSk7IiwiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuY29udHJvbGxlcignYm9va2luZ2RldGFpbHNDdHJsJywgZnVuY3Rpb24oKSB7XHJcblx0XHR2YXIgY3RybCA9IHRoaXM7XHJcblx0XHRjdHJsLnQgPSB7fTtcclxuXHR9KTsiLCJhbmd1bGFyLm1vZHVsZSgnZmlyc3RBcHAnKVxyXG5cdC5kaXJlY3RpdmUoJ2Jvb2tpbmdkZXRhaWxzJywgZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRyZXN0cmljdDogJ0VBJyxcclxuXHRcdFx0c2NvcGU6IHt9LFxyXG5cdFx0XHRjb250cm9sbGVyOiAnYm9va2luZ2RldGFpbHNDdHJsJyxcclxuXHRcdFx0Y29udHJvbGxlckFzOiAnY3RybCcsXHJcblx0XHRcdGJpbmRUb0NvbnRyb2xsZXI6IHt9LFxyXG5cdFx0XHR0ZW1wbGF0ZVVybDogJy4vdGVtcGxhdGVzL2Jvb2tpbmdkZXRhaWxzLmh0bWwnXHJcblx0XHR9O1xyXG5cdH0pOyIsImFuZ3VsYXIubW9kdWxlKCdmaXJzdEFwcCcpXHJcblx0LmNvbnRyb2xsZXIoJ2Jvb2tpbmd0aGFua3lvdUN0cmwnLCBmdW5jdGlvbigpIHtcclxuXHRcdHZhciBjdHJsID0gdGhpcztcclxuXHRcdGN0cmwudCA9IHt9O1xyXG5cdH0pOyIsImFuZ3VsYXIubW9kdWxlKCdmaXJzdEFwcCcpXHJcblx0LmRpcmVjdGl2ZSgnYm9va2luZ3RoYW5reW91JywgZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRyZXN0cmljdDogJ0VBJyxcclxuXHRcdFx0c2NvcGU6IHt9LFxyXG5cdFx0XHRjb250cm9sbGVyOiAnYm9va2luZ3RoYW5reW91Q3RybCcsXHJcblx0XHRcdGNvbnRyb2xsZXJBczogJ2N0cmwnLFxyXG5cdFx0XHRiaW5kVG9Db250cm9sbGVyOiB7fSxcclxuXHRcdFx0dGVtcGxhdGVVcmw6ICcuL3RlbXBsYXRlcy9ib29raW5ndGhhbmt5b3UuaHRtbCdcclxuXHRcdH07XHJcblx0fSk7IiwiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuY29udHJvbGxlcignYnJlYWRjcnVtc0N0cmwnLCBmdW5jdGlvbigpIHtcclxuXHRcdHZhciBjdHJsID0gdGhpcztcclxuXHRcdGN0cmwudCA9IHt9O1xyXG5cdH0pOyIsImFuZ3VsYXIubW9kdWxlKCdmaXJzdEFwcCcpXHJcblx0LmRpcmVjdGl2ZSgnYnJlYWRjcnVtcycsIGZ1bmN0aW9uKCkge1xyXG5cdFx0cmV0dXJuIHtcclxuXHRcdFx0cmVzdHJpY3Q6ICdFQScsXHJcblx0XHRcdHNjb3BlOiB7fSxcclxuXHRcdFx0Y29udHJvbGxlcjogJ2JyZWFkY3J1bXNDdHJsJyxcclxuXHRcdFx0Y29udHJvbGxlckFzOiAnY3RybCcsXHJcblx0XHRcdGJpbmRUb0NvbnRyb2xsZXI6IHt9LFxyXG5cdFx0XHR0ZW1wbGF0ZVVybDogJy4vdGVtcGxhdGVzL2JyZWFkY3J1bXMuaHRtbCdcclxuXHRcdH07XHJcblx0fSk7IiwiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuY29udHJvbGxlcignaG9tZUN0cmwnLCBmdW5jdGlvbiAoQ2l0eVNlcnZpY2UsICRyb3V0ZVBhcmFtcywgJHdpbmRvdywgJHNjb3BlKSB7XHJcblx0XHR2YXIgY3RybCA9IHRoaXM7XHJcblx0XHRjdHJsLnQgPSB7XHJcblx0XHRcdHRpdGxlOiAnSG9tZSBwYWdlJyxcclxuXHRcdH07XHJcblx0XHRjdHJsLmNpdGllcyA9IFtdO1xyXG5cdFx0Y3RybC5kYXRlUGlja2VyID0ge1xyXG5cdFx0XHRzdGFydERhdGU6IG51bGwsXHJcblx0XHRcdGVuZERhdGU6IG51bGxcclxuXHRcdH07XHJcblxyXG5cdFx0Y3RybC5hZ2VsaXN0ID0gW3tcclxuXHRcdFx0bGFiZWw6ICcxJyxcclxuXHRcdFx0dmFsdWU6ICcxJ1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICcyJyxcclxuXHRcdFx0dmFsdWU6ICcyJ1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICczJyxcclxuXHRcdFx0dmFsdWU6ICczJ1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICc0JyxcclxuXHRcdFx0dmFsdWU6ICc0J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICc1JyxcclxuXHRcdFx0dmFsdWU6ICc1J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICc2JyxcclxuXHRcdFx0dmFsdWU6ICc2J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICc3JyxcclxuXHRcdFx0dmFsdWU6ICc3J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICc4JyxcclxuXHRcdFx0dmFsdWU6ICc4J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICc5JyxcclxuXHRcdFx0dmFsdWU6ICc5J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bGFiZWw6ICcxMCcsXHJcblx0XHRcdHZhbHVlOiAnMTAnXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRsYWJlbDogJzExJyxcclxuXHRcdFx0dmFsdWU6ICcxMSdcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdGxhYmVsOiAnMTInLFxyXG5cdFx0XHR2YWx1ZTogJzEyJ1xyXG5cdFx0fSxcclxuXHRcdF07XHJcblxyXG5cdFx0Y3RybC5kYXRlUGlja2VyT3B0aW5zID0ge1xyXG5cdFx0XHRsb2NhbGU6IHtcclxuXHRcdFx0XHRhcHBseUxhYmVsOiAnQXBwbHknLFxyXG5cdFx0XHRcdGZyb21MYWJlbDogJ0NoZWNrIEluJyxcclxuXHRcdFx0XHQvLy9mb3JtYXQ6ICdZWVlZLU1NLUREJywgLy93aWxsIGdpdmUgeW91IDIwMTctMDEtMDZcclxuXHRcdFx0XHQvL2Zvcm1hdDogJ0QtTU1NLVlZJywgLy93aWxsIGdpdmUgeW91IDYtSmFuLTE3XHJcblx0XHRcdFx0Zm9ybWF0OiAnREQtTU0tWVlZWScsIC8vd2lsbCBnaXZlIHlvdSA2LUphbnVhcnktMTdcclxuXHRcdFx0XHR0b0xhYmVsOiAnQ2hlY2sgT3V0JyxcclxuXHRcdFx0XHRjYW5jZWxMYWJlbDogJ0NhbmNlbCcsXHJcblx0XHRcdFx0c2VwYXJhdG9yOiAnIHRvICdcclxuXHRcdFx0XHQvL2N1c3RvbVJhbmdlTGFiZWw6ICdDdXN0b20gcmFuZ2UnXHJcblx0XHRcdH1cclxuXHRcdH07XHJcblxyXG5cdFx0Y3RybC5ob21lYm9va25vdyA9IHtcclxuXHRcdFx0dHlwZTogJ2hvdGVsJyxcclxuXHRcdFx0bG9jYXRpb246ICdCQU5HQUxPUkUnLFxyXG5cdFx0XHRjaGVja2luOiBuZXcgRGF0ZSgpLFxyXG5cdFx0XHRjaGVja291dDogbmV3IERhdGUobmV3IERhdGUoKS5zZXREYXRlKG5ldyBEYXRlKCkuZ2V0RGF0ZSgpICsgMTApKSxcclxuXHRcdFx0cm9vbXM6IFt7XHJcblx0XHRcdFx0YWR1bHRzOiAxLFxyXG5cdFx0XHRcdGNoaWxkczogMCxcclxuXHRcdFx0XHRjaGlsZEFnZXM6IFtdLFxyXG5cdFx0XHR9XSxcclxuXHRcdFx0Ly8gYWR1bHRzOiAxLFxyXG5cdFx0XHQvLyBjaGlsZDogMCxcclxuXHRcdFx0Ly8gY2hpbGRBZ2U6IFtdLFxyXG5cdFx0fTtcclxuXHJcblx0XHRDaXR5U2VydmljZS5nZXRBbGwoKS50aGVuKGZ1bmN0aW9uIChjaXRpZXMpIHtcclxuXHRcdFx0Y3RybC5jaXRpZXMgPSBjaXRpZXM7XHJcblx0XHRcdGNvbnNvbGUubG9nKGN0cmwuY2l0aWVzKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdGN0cmwudXBkYXRlTG9jYXRpb24gPSBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdGNvbnNvbGUubG9nKGN0cmwubG9jYXRpb24pO1xyXG5cdFx0XHRjdHJsLmhvbWVib29rbm93LmxvY2F0aW9uID0gY3RybC5sb2NhdGlvbi5jYTtcclxuXHRcdH07XHJcblxyXG5cdFx0Y3RybC5hZGRNb3JlID0gZnVuY3Rpb24gKHR5cGUsIGluZGV4KSB7XHJcblx0XHRcdGNvbnNvbGUubG9nKGluZGV4KTtcclxuXHRcdFx0aWYgKHR5cGUgPT0gJ3Jvb21zJykge1xyXG5cdFx0XHRcdGN0cmwuaG9tZWJvb2tub3cucm9vbXMucHVzaCh7XHJcblx0XHRcdFx0XHRhZHVsdHM6IDEsXHJcblx0XHRcdFx0XHRjaGlsZHM6IDAsXHJcblx0XHRcdFx0XHRjaGlsZEFnZXM6IFtdLFxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9IGVsc2UgaWYgKHR5cGUgPT0gJ2FkdWx0cycpIHtcclxuXHRcdFx0XHRjb25zb2xlLmxvZygnaSBjYW1lIGhlcmUnKTtcclxuXHRcdFx0XHRjb25zb2xlLmxvZyh0eXBlKVxyXG5cdFx0XHRcdGNvbnNvbGUubG9nKGluZGV4KVxyXG5cdFx0XHRcdGNvbnNvbGUubG9nKGN0cmwuaG9tZWJvb2tub3cucm9vbXNbaW5kZXhdKTtcclxuXHRcdFx0XHRpZiAocGFyc2VJbnQoY3RybC5ob21lYm9va25vdy5yb29tc1tpbmRleF1bdHlwZV0pID4gMCkge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coJ2NvbnNvbGUubG9nJylcclxuXHRcdFx0XHRcdGN0cmwuaG9tZWJvb2tub3cucm9vbXNbaW5kZXhdW3R5cGVdID0gcGFyc2VJbnQoY3RybC5ob21lYm9va25vdy5yb29tc1tpbmRleF1bdHlwZV0gKyAxKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSBpZiAodHlwZSA9PSAnY2hpbGRzJykge1xyXG5cdFx0XHRcdGlmIChwYXJzZUludChjdHJsLmhvbWVib29rbm93LnJvb21zW2luZGV4XVt0eXBlXSkgPj0gMCkge1xyXG5cdFx0XHRcdFx0Y3RybC5ob21lYm9va25vdy5yb29tc1tpbmRleF1bdHlwZV0gPSBwYXJzZUludChjdHJsLmhvbWVib29rbm93LnJvb21zW2luZGV4XVt0eXBlXSArIDEpO1xyXG5cdFx0XHRcdFx0Y3RybC5ob21lYm9va25vdy5yb29tc1tpbmRleF0uY2hpbGRBZ2VzLnB1c2goe1xyXG5cdFx0XHRcdFx0XHRhZ2U6ICcxJ1xyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFx0Y3RybC5yZW1vdmVTb21lID0gZnVuY3Rpb24gKHR5cGUsIGluZGV4KSB7XHJcblx0XHRcdGNvbnNvbGUubG9nKGluZGV4KTtcclxuXHRcdFx0aWYgKHR5cGUgPT0gJ3Jvb21zJykge1xyXG5cdFx0XHRcdGlmICghKHBhcnNlSW50KGN0cmwuaG9tZWJvb2tub3dbdHlwZV0pIDw9IDEpKSB7XHJcblx0XHRcdFx0XHRjdHJsLmhvbWVib29rbm93LnJvb21zID0gY3RybC5ob21lYm9va25vdy5yb29tcy5zbGljZSgwLCAtMSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2UgaWYgKHR5cGUgPT0gJ2FkdWx0cycpIHtcclxuXHRcdFx0XHRpZiAoIShwYXJzZUludChjdHJsLmhvbWVib29rbm93LnJvb21zW2luZGV4XVt0eXBlXSkgPD0gMSkpIHtcclxuXHRcdFx0XHRcdGN0cmwuaG9tZWJvb2tub3cucm9vbXNbaW5kZXhdW3R5cGVdID0gcGFyc2VJbnQoY3RybC5ob21lYm9va25vdy5yb29tc1tpbmRleF1bdHlwZV0gLSAxKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSBpZiAodHlwZSA9PSAnY2hpbGRzJykge1xyXG5cdFx0XHRcdGlmICghKHBhcnNlSW50KGN0cmwuaG9tZWJvb2tub3cucm9vbXNbaW5kZXhdW3R5cGVdKSA8PSAwKSkge1xyXG5cdFx0XHRcdFx0Y3RybC5ob21lYm9va25vdy5yb29tc1tpbmRleF1bdHlwZV0gPSBwYXJzZUludChjdHJsLmhvbWVib29rbm93LnJvb21zW2luZGV4XVt0eXBlXSAtIDEpO1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coJ3JlbW92aW5nIHBhcnQnKTtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGN0cmwuaG9tZWJvb2tub3cuY2hpbGRBZ2VzKTtcclxuXHRcdFx0XHRcdGN0cmwuaG9tZWJvb2tub3cucm9vbXNbaW5kZXhdLmNoaWxkQWdlcyA9IGN0cmwuaG9tZWJvb2tub3cucm9vbXNbaW5kZXhdLmNoaWxkQWdlcy5zbGljZSgwLCAtMSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cclxuXHRcdGN0cmwuYm9va25vdyA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coJyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKicpO1xyXG5cdFx0XHRjb25zb2xlLmxvZyhjdHJsLmhvbWVib29rbm93KTtcclxuXHRcdFx0dmFyIGNoZWNraW5kYXRlID0gbW9tZW50KGN0cmwuaG9tZWJvb2tub3cuY2hlY2tpbikuZm9ybWF0KCdERC1NTS1ZWVlZJyk7XHJcblx0XHRcdHZhciBjaGVja291dGRhdGUgPSBtb21lbnQoY3RybC5ob21lYm9va25vdy5jaGVja291dCkuZm9ybWF0KCdERC1NTS1ZWVlZJyk7XHJcblxyXG5cclxuXHRcdFx0Y3RybC5ob21lYm9va25vdy5yb29tcy5tYXAoZnVuY3Rpb24gKGl0ZW0sIGluZGV4KSB7XHJcblx0XHRcdFx0Y29uc29sZS5sb2coaXRlbSk7XHJcblx0XHRcdFx0dmFyIHN0ciA9ICcnO1xyXG5cdFx0XHRcdGZvciAodmFyIGtleSBpbiBpdGVtKSB7XHJcblx0XHRcdFx0XHRjb25zb2xlLmxvZyhrZXksICdrZXknKVxyXG5cdFx0XHRcdFx0aWYgKHN0ciAhPSAnJykge1xyXG5cdFx0XHRcdFx0XHRzdHIgKz0gJyYnO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKGtleSA9PSAnY2hpbGRBZ2VzJykge1xyXG5cdFx0XHRcdFx0XHR2YXIgY2hpbGRhZ2UgPSBbXTtcclxuXHRcdFx0XHRcdFx0aXRlbVtrZXldLm1hcChmdW5jdGlvbiAoaXRlbSkge1xyXG5cdFx0XHRcdFx0XHRcdGNoaWxkYWdlLnB1c2goaXRlbS5hZ2UpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2coY2hpbGRhZ2UpO1xyXG5cdFx0XHRcdFx0XHRzdHIgKz0gZW5jb2RlVVJJQ29tcG9uZW50KGByb29tWyR7aW5kZXh9XS5gKSArIGtleSArICc9JyArIGNoaWxkYWdlLnRvU3RyaW5nKCk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRzdHIgKz0gZW5jb2RlVVJJQ29tcG9uZW50KGByb29tWyR7aW5kZXh9XS5gKSArIGtleSArICc9JyArIGVuY29kZVVSSUNvbXBvbmVudChpdGVtW2tleV0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRjdHJsLnJvdXRlcnBhcmFtdXJsc3RyID0gc3RyO1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKHN0cik7XHJcblx0XHRcdFx0Y29uc29sZS5sb2coJyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKicpO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdC8qdmFyIHN0ciA9ICcnO1xyXG5cdFx0XHRmb3IgKHZhciBrZXkgaW4gY3RybC5ob21lYm9va25vdy5yb29tcykge1xyXG5cdFx0XHRcdGlmIChzdHIgIT0gJycpIHtcclxuXHRcdFx0XHRcdHN0ciArPSAnJic7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHN0ciArPSBrZXkgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQoY3RybC5ob21lYm9va25vdy5yb29tc1trZXldKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRjb25zb2xlLmxvZyhzdHIpO1xyXG5cdFx0XHRjb25zb2xlLmxvZygnKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqJyk7Ki9cclxuXHJcblx0XHRcdC8vdmFyIHN0cmluZyA9ICcmJ1xyXG5cclxuXHJcblx0XHRcdC8vdmFyIHBhcmFtU3RyaW5nID0gYGh0dHA6Ly9ob2xpZGF5c2ZvcmFsbC5pbi90cmlwc29sdXRpb25zL3JhdGUvaG90ZWwvZGV0YWlsP2NoZWNrSW5EYXRlPSR7Y2hlY2tpbmRhdGV9JmNoZWNrT3V0RGF0ZT0ke2NoZWNrb3V0ZGF0ZX0mcm9vbXMlNUIke2N0cmwuaG9tZWJvb2tub3cucm9vbXN9JTVELmFkdWx0cz0yJmNpdHk9JHtjdHJsLmhvbWVib29rbm93LmxvY2F0aW9ufSZyb29tcyU1QjElNUQuYWR1bHRzPTMmaG90ZWxpZD0ke2N0cmwuaG9tZWJvb2tub3cuaG90ZWxpZH0mcmF0ZVBsYW5JZD0wMDAxMzEyNTE1JnJvb21UeXBlSWQ9MDAwMDA0NTYzNiZyb29tcyU1QjAlNUQuY2hpbGRBZ2VzPTUsMTAmcm9vbXMlNUIwJTVELmNoaWxkcz0yYDtcclxuXHRcdFx0dmFyIHBhcmFtU3RyaW5nID0gYGNoZWNrSW5EYXRlPSR7Y2hlY2tpbmRhdGV9JmNoZWNrT3V0RGF0ZT0ke2NoZWNrb3V0ZGF0ZX0mY2l0eT0ke2N0cmwuaG9tZWJvb2tub3cubG9jYXRpb259JmA7XHJcblx0XHRcdGNvbnNvbGUubG9nKHBhcmFtU3RyaW5nKTtcclxuXHRcdFx0JHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9jaXR5LycrIGN0cmwuaG9tZWJvb2tub3cubG9jYXRpb24udG9Mb3dlckNhc2UoKSArIGA/Y2hlY2tJbkRhdGU9JHtjaGVja2luZGF0ZX0mY2hlY2tPdXREYXRlPSR7Y2hlY2tvdXRkYXRlfSZjaXR5PSR7Y3RybC5ob21lYm9va25vdy5sb2NhdGlvbn0mYCArIGN0cmwucm91dGVycGFyYW11cmxzdHI7XHJcblx0XHR9O1xyXG5cclxuXHRcdGN0cmwudXBkYXRlQ2hlY2tJbk91dCA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coY3RybC5kYXRlUGlja2VyKTtcclxuXHRcdH07XHJcblxyXG5cdFx0Y3RybC5zaG93RGF0ZVBpY2tlciA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0JCgnI2RhdGVSYW5nZVBpY2tlcmhpZGUnKS50cmlnZ2VyKCdjbGljaycpO1xyXG5cdFx0fTtcclxuXHJcblx0XHQkc2NvcGUuJHdhdGNoKCdjdHJsLmRhdGVQaWNrZXInLCBmdW5jdGlvbiAobmV3VmFsKSB7XHJcblx0XHRcdGNvbnNvbGUubG9nKG5ld1ZhbCk7XHJcblx0XHRcdGlmIChuZXdWYWwuc3RhcnREYXRlICYmIG5ld1ZhbC5lbmREYXRlKSB7XHJcblx0XHRcdFx0Y3RybC5ob21lYm9va25vdy5jaGVja2luID0gbmV3VmFsLnN0YXJ0RGF0ZS5fZDtcclxuXHRcdFx0XHRjdHJsLmhvbWVib29rbm93LmNoZWNrb3V0ID0gbmV3VmFsLmVuZERhdGUuX2Q7XHJcblx0XHRcdH1cclxuXHRcdH0sIHRydWUpO1xyXG5cclxuXHR9KTsiLCJcclxuYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuZGlyZWN0aXZlKCdob21lJywgZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRyZXN0cmljdDogJ0VBJyxcclxuXHRcdFx0c2NvcGU6IHt9LFxyXG5cdFx0XHRjb250cm9sbGVyOiAnaG9tZUN0cmwnLFxyXG5cdFx0XHRjb250cm9sbGVyQXM6ICdjdHJsJyxcclxuXHRcdFx0YmluZFRvQ29udHJvbGxlcjoge30sXHJcblx0XHRcdHRlbXBsYXRlVXJsOiAnLi90ZW1wbGF0ZXMvaG9tZS5odG1sJ1xyXG5cdFx0fTtcclxuXHR9KTsiLCJhbmd1bGFyLm1vZHVsZSgnZmlyc3RBcHAnKVxyXG5cdC5jb250cm9sbGVyKCdob3RlbHBhZ2VDdHJsJywgZnVuY3Rpb24oJHJvdXRlUGFyYW1zLCAkc2NvcGUsIEhvdGVsU2VydmljZSwgSG90ZWxQcmljaW5nU2VydmljZSkge1xyXG5cdFx0dmFyIGN0cmwgPSB0aGlzO1xyXG5cdFx0Y3RybC50ID0ge307XHJcblx0XHRjdHJsLnJvdXRlUGFyYW1zID0gJHJvdXRlUGFyYW1zO1xyXG5cdFx0Y3RybC5wcmljaW5nID0ge307XHJcblx0XHRjdHJsLmhvdGVsID0ge307XHJcblxyXG5cdFx0SG90ZWxTZXJ2aWNlLmdldEFsbChjdHJsLnJvdXRlUGFyYW1zLmhvdGVsaWQpLnRoZW4oZnVuY3Rpb24oaG90ZWwpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coaG90ZWwpO1xyXG5cdFx0XHRjdHJsLmhvdGVsID0gaG90ZWw7XHJcblx0XHR9KTtcclxuXHJcblx0XHRIb3RlbFByaWNpbmdTZXJ2aWNlLmdldEFsbCgpLnRoZW4oZnVuY3Rpb24ocHJpY2luZykge1xyXG5cdFx0XHRjb25zb2xlLmxvZygnaG90ZWxwcmljaW5nPT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcclxuXHRcdFx0Y29uc29sZS5sb2cocHJpY2luZyk7XHJcblx0XHRcdGN0cmwucHJpY2luZyA9IHByaWNpbmc7XHJcblx0XHR9KTtcclxuXHJcblx0fSk7IiwiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuZGlyZWN0aXZlKCdob3RlbHBhZ2UnLCBmdW5jdGlvbigpIHtcclxuXHRcdHJldHVybiB7XHJcblx0XHRcdHJlc3RyaWN0OiAnRUEnLFxyXG5cdFx0XHRzY29wZToge30sXHJcblx0XHRcdGNvbnRyb2xsZXI6ICdob3RlbHBhZ2VDdHJsJyxcclxuXHRcdFx0Y29udHJvbGxlckFzOiAnY3RybCcsXHJcblx0XHRcdGJpbmRUb0NvbnRyb2xsZXI6IHt9LFxyXG5cdFx0XHR0ZW1wbGF0ZVVybDogJy4vdGVtcGxhdGVzL2hvdGVscGFnZS5odG1sJ1xyXG5cdFx0fTtcclxuXHR9KTsiLCJhbmd1bGFyLm1vZHVsZSgnZmlyc3RBcHAnKVxyXG5cdC5jb250cm9sbGVyKCduYXZpZ2F0aW9uQ3RybCcsIGZ1bmN0aW9uKCkge1xyXG5cdFx0dmFyIGN0cmwgPSB0aGlzO1xyXG5cdFx0Y3RybC50ID0ge307XHJcblx0fSk7IiwiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuZGlyZWN0aXZlKCduYXZpZ2F0aW9uJywgZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRyZXN0cmljdDogJ0VBJyxcclxuXHRcdFx0c2NvcGU6IHt9LFxyXG5cdFx0XHRjb250cm9sbGVyOiAnbmF2aWdhdGlvbkN0cmwnLFxyXG5cdFx0XHRjb250cm9sbGVyQXM6ICdjdHJsJyxcclxuXHRcdFx0YmluZFRvQ29udHJvbGxlcjoge30sXHJcblx0XHRcdHRlbXBsYXRlVXJsOiAnLi90ZW1wbGF0ZXMvbmF2aWdhdGlvbi5odG1sJ1xyXG5cdFx0fTtcclxuXHR9KTsiLCJhbmd1bGFyLm1vZHVsZSgnZmlyc3RBcHAnKVxyXG5cdC5jb250cm9sbGVyKCdzZWFyY2hyZXN1bHRzQ3RybCcsIGZ1bmN0aW9uICgkcm91dGVQYXJhbXMsICRzY29wZSwgQ2l0eWhvdGVsc1NlcnZpY2UsICR3aW5kb3csICRodHRwKSB7XHJcblx0XHR2YXIgY3RybCA9IHRoaXM7XHJcblx0XHRjdHJsLnQgPSB7fTtcclxuXHRcdGN0cmwucm91dGVQYXJhbXMgPSAkcm91dGVQYXJhbXM7XHJcblx0XHRjdHJsLmNpdHlob3RlbHMgPSBbXTtcclxuXHJcblx0XHRjdHJsLnVybGRhdGEgPSB7fTtcclxuXHJcblx0XHRDaXR5aG90ZWxzU2VydmljZS5nZXRBbGwoY3RybC5yb3V0ZVBhcmFtcy5jaXR5bmFtZSkudGhlbihmdW5jdGlvbiAoY2l0eWhvdGVscykge1xyXG5cdFx0XHRjb25zb2xlLmxvZygnY2l0eWhvdGVscz09PT09PT09PT09PT09PT09PT09Jyk7XHJcblx0XHRcdGNvbnNvbGUubG9nKGNpdHlob3RlbHMpO1xyXG5cdFx0XHRjdHJsLmNpdHlob3RlbHMgPSBjaXR5aG90ZWxzO1xyXG5cclxuXHRcdFx0Y3RybC5jaXR5aG90ZWxzLm1hcChmdW5jdGlvbiAoaXRlbSkge1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKGl0ZW0udmlkKTtcclxuXHRcdFx0XHQkaHR0cCh7XHJcblx0XHRcdFx0XHRtZXRob2Q6ICdHRVQnLFxyXG5cdFx0XHRcdFx0dXJsOiAnaHR0cDovL2hvbGlkYXlzZm9yYWxsLmluL3RyaXBzb2x1dGlvbnMvcmF0ZS9ob3RlbC9kZXRhaWwnICsgJHdpbmRvdy5sb2NhdGlvbi5zZWFyY2ggKyAnJmhvdGVsaWQ9JyArIGl0ZW0udmlkLFxyXG5cdFx0XHRcdFx0aGVhZGVyczoge1xyXG5cdFx0XHRcdFx0XHQnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRkYXRhOiB7fVxyXG5cdFx0XHRcdH0pLnRoZW4oXHJcblx0XHRcdFx0XHRmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ3Jlc3BvbnplJywgcmVzcG9uc2UpO1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdGZ1bmN0aW9uIChlcnJvcikge1xyXG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZygnZXJyb3InLCBlcnJvcik7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0fSk7XHJcblxyXG5cdFx0JHNjb3BlLiR3YXRjaCgnY3RybC5yb3V0ZVBhcmFtcycsIGZ1bmN0aW9uIChuZXdWYWwpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coJ3JvdXRlciBwYXJhbXMnKTtcclxuXHRcdFx0Y29uc29sZS5sb2cobmV3VmFsKTtcclxuXHJcblx0XHRcdGNvbnNvbGUubG9nKCR3aW5kb3cubG9jYXRpb24uc2VhcmNoKTtcclxuXHJcblx0XHR9KSwgdHJ1ZTtcclxuXHR9KVxyXG5cdC5maWx0ZXIoJ2N1dCcsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHJldHVybiBmdW5jdGlvbiAodmFsdWUsIHdvcmR3aXNlLCBtYXgsIHRhaWwpIHtcclxuXHRcdFx0aWYgKCF2YWx1ZSkgcmV0dXJuICcnO1xyXG5cclxuXHRcdFx0bWF4ID0gcGFyc2VJbnQobWF4LCAxMCk7XHJcblx0XHRcdGlmICghbWF4KSByZXR1cm4gdmFsdWU7XHJcblx0XHRcdGlmICh2YWx1ZS5sZW5ndGggPD0gbWF4KSByZXR1cm4gdmFsdWU7XHJcblxyXG5cdFx0XHR2YWx1ZSA9IHZhbHVlLnN1YnN0cigwLCBtYXgpO1xyXG5cdFx0XHRpZiAod29yZHdpc2UpIHtcclxuXHRcdFx0XHR2YXIgbGFzdHNwYWNlID0gdmFsdWUubGFzdEluZGV4T2YoJyAnKTtcclxuXHRcdFx0XHRpZiAobGFzdHNwYWNlICE9PSAtMSkge1xyXG5cdFx0XHRcdFx0Ly9BbHNvIHJlbW92ZSAuIGFuZCAsIHNvIGl0cyBnaXZlcyBhIGNsZWFuZXIgcmVzdWx0LlxyXG5cdFx0XHRcdFx0aWYgKHZhbHVlLmNoYXJBdChsYXN0c3BhY2UgLSAxKSA9PT0gJy4nIHx8IHZhbHVlLmNoYXJBdChsYXN0c3BhY2UgLSAxKSA9PT0gJywnKSB7XHJcblx0XHRcdFx0XHRcdGxhc3RzcGFjZSA9IGxhc3RzcGFjZSAtIDE7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR2YWx1ZSA9IHZhbHVlLnN1YnN0cigwLCBsYXN0c3BhY2UpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHZhbHVlICsgKHRhaWwgfHwgJyDigKYnKTtcclxuXHRcdH07XHJcblx0fSk7IiwiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuZGlyZWN0aXZlKCdzZWFyY2hyZXN1bHRzJywgZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRyZXN0cmljdDogJ0VBJyxcclxuXHRcdFx0c2NvcGU6IHt9LFxyXG5cdFx0XHRjb250cm9sbGVyOiAnc2VhcmNocmVzdWx0c0N0cmwnLFxyXG5cdFx0XHRjb250cm9sbGVyQXM6ICdjdHJsJyxcclxuXHRcdFx0YmluZFRvQ29udHJvbGxlcjoge30sXHJcblx0XHRcdHRlbXBsYXRlVXJsOiAnLi90ZW1wbGF0ZXMvc2VhcmNocmVzdWx0cy5odG1sJ1xyXG5cdFx0fTtcclxuXHR9KTsiLCJhbmd1bGFyLm1vZHVsZSgnZmlyc3RBcHAnKVxyXG5cdC5mYWN0b3J5KCdIb3RlbCcsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHJldHVybiBmdW5jdGlvbiBIb3RlbChob3RlbCkge1xyXG5cdFx0XHRhbmd1bGFyLmV4dGVuZCh0aGlzLCBob3RlbCk7XHJcblx0XHRcdGNvbnNvbGUubG9nKGhvdGVsKTtcclxuXHRcdH07XHJcblx0fSlcclxuXHQuZmFjdG9yeSgnQ2l0eWhvdGVsc1NlcnZpY2UnLCBmdW5jdGlvbiAoJHEsICRodHRwLCBIb3RlbCkge1xyXG5cclxuXHRcdHZhciBjaXR5aG90ZWxzID0gW107XHJcblx0XHR2YXIgbG9hZFByb21pc2UgPSB1bmRlZmluZWQ7XHJcblx0XHR2YXIgbG9hZEFsbCA9IGZ1bmN0aW9uIChjaXR5bmFtZSkge1xyXG5cdFx0XHRpZiAoY2l0eWhvdGVscy5sb2FkUHJvbWlzZSA+IDApIHtcclxuXHRcdFx0XHRyZXR1cm4gJHEud2hlbihjaXR5aG90ZWxzKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoXy5pc1VuZGVmaW5lZChsb2FkUHJvbWlzZSkpIHtcclxuXHRcdFx0XHRsb2FkUHJvbWlzZSA9ICRodHRwKHtcclxuXHRcdFx0XHRcdG1ldGhvZDogJ0dFVCcsXHJcblx0XHRcdFx0XHR1cmw6ICdodHRwOi8vaG9saWRheXNmb3JhbGwuaW4vdHJpcHNvbHV0aW9ucy9jb250ZW50L2NpdHk/Y2l0eU5hbWU9JysgY2l0eW5hbWUudG9VcHBlckNhc2UoKSxcclxuXHRcdFx0XHRcdGRhdGE6IHt9XHJcblx0XHRcdFx0fSkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xyXG5cdFx0XHRcdFx0bG9hZFByb21pc2UgPSB1bmRlZmluZWQ7XHJcblx0XHRcdFx0XHRjb25zb2xlLmxvZyhkYXRhKTtcclxuXHRcdFx0XHRcdGlmKGRhdGEuc3RhdHVzID09PSAyMDApIHtcclxuXHRcdFx0XHRcdFx0aWYoZGF0YS5kYXRhLmRhdGEpIHtcclxuXHRcdFx0XHRcdFx0XHRjaXR5aG90ZWxzID0gZGF0YS5kYXRhLmRhdGEubWFwKGZ1bmN0aW9uIChob3RlbCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuIG5ldyBIb3RlbChob3RlbCk7XHJcblx0XHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gbG9hZFByb21pc2U7XHJcblx0XHR9O1xyXG5cclxuXHRcdHJldHVybiB7XHJcblx0XHRcdGdldEFsbDogZnVuY3Rpb24gKGNpdHluYW1lKSB7XHJcblx0XHRcdFx0cmV0dXJuIGxvYWRBbGwoY2l0eW5hbWUpLnRoZW4oZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGNpdHlob3RlbHM7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblxyXG5cdH0pOyIsImFuZ3VsYXIubW9kdWxlKCdmaXJzdEFwcCcpXG5cdC5mYWN0b3J5KCdDaXR5JywgZnVuY3Rpb24gKCkge1xuXHRcdHJldHVybiBmdW5jdGlvbiBDaXR5KGNpdHkpIHtcblx0XHRcdGFuZ3VsYXIuZXh0ZW5kKHRoaXMsIGNpdHkpO1xuXHRcdH07XG5cdH0pXG5cdC5mYWN0b3J5KCdDaXR5U2VydmljZScsIGZ1bmN0aW9uICgkcSwgJGh0dHAsIENpdHkpIHtcblxuXHRcdHZhciBjaXRpZXMgPSBbXTtcblx0XHR2YXIgbG9hZFByb21pc2UgPSB1bmRlZmluZWQ7XG5cdFx0dmFyIGxvYWRBbGwgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRpZiAoY2l0aWVzLmxvYWRQcm9taXNlID4gMCkge1xuXHRcdFx0XHRyZXR1cm4gJHEud2hlbihjaXRpZXMpO1xuXHRcdFx0fVxuXHRcdFx0aWYgKF8uaXNVbmRlZmluZWQobG9hZFByb21pc2UpKSB7XG5cdFx0XHRcdGxvYWRQcm9taXNlID0gJGh0dHAoe1xuXHRcdFx0XHRcdG1ldGhvZDogJ0dFVCcsXG5cdFx0XHRcdFx0dXJsOiAnaHR0cDovL2hvbGlkYXlzZm9yYWxsLmluL3RyaXBzb2x1dGlvbnMvY2l0aWVzJyxcblx0XHRcdFx0XHRkYXRhOiB7fVxuXHRcdFx0XHR9KS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG5cdFx0XHRcdFx0bG9hZFByb21pc2UgPSB1bmRlZmluZWQ7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2coZGF0YSk7XG5cdFx0XHRcdFx0aWYoZGF0YS5zdGF0dXMgPT09IDIwMCkge1xuXHRcdFx0XHRcdFx0Y2l0aWVzID0gZGF0YS5kYXRhLmRhdGEubWFwKGZ1bmN0aW9uIChjaXR5KSB7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBuZXcgQ2l0eShjaXR5KTtcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gbG9hZFByb21pc2U7XG5cdFx0fTtcblxuXHRcdHJldHVybiB7XG5cdFx0XHRnZXRBbGw6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0cmV0dXJuIGxvYWRBbGwoKS50aGVuKGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRyZXR1cm4gY2l0aWVzO1xuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHR9O1xuXG5cdH0pOyIsImFuZ3VsYXIubW9kdWxlKCdmaXJzdEFwcCcpXHJcblx0LmZhY3RvcnkoJ0hvdGVsUHJpY2luZycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHJldHVybiBmdW5jdGlvbiBIb3RlbFByaWNpbmcoaG90ZWxwcmljZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaSBjYW1lIGhlcmUnKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coaG90ZWxwcmljZSk7XHJcblx0XHRcdGFuZ3VsYXIuZXh0ZW5kKHRoaXMsIGhvdGVscHJpY2UpO1xyXG5cdFx0fTtcclxuXHR9KVxyXG5cdC5mYWN0b3J5KCdIb3RlbFByaWNpbmdTZXJ2aWNlJywgZnVuY3Rpb24gKCRxLCAkaHR0cCwgSG90ZWxQcmljaW5nKSB7XHJcblxyXG5cdFx0dmFyIGhvdGVscHJpY2UgPSBbXTtcclxuXHRcdHZhciBsb2FkUHJvbWlzZSA9IHVuZGVmaW5lZDtcclxuXHRcdHZhciBsb2FkQWxsID0gZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRpZiAoaG90ZWxwcmljZS5sb2FkUHJvbWlzZSA+IDApIHtcclxuXHRcdFx0XHRyZXR1cm4gJHEud2hlbihob3RlbHByaWNlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoXy5pc1VuZGVmaW5lZChsb2FkUHJvbWlzZSkpIHtcclxuXHRcdFx0XHRsb2FkUHJvbWlzZSA9ICRodHRwKHtcclxuXHRcdFx0XHRcdG1ldGhvZDogJ0dFVCcsXHJcblx0XHRcdFx0XHR1cmw6ICdodHRwOi8vaG9saWRheXNmb3JhbGwuaW4vdHJpcHNvbHV0aW9ucy9yYXRlL2hvdGVsL2RldGFpbD9jaGVja0luRGF0ZT0wNS0wOS0yMDE4JmNoZWNrT3V0RGF0ZT0wNi0wOS0yMDE4JnJvb21zJTVCMCU1RC5hZHVsdHM9MiZjaXR5PUJhbmdhbG9yZSZyb29tcyU1QjElNUQuYWR1bHRzPTMmaG90ZWxpZD0wMDAxMjg2NCZyYXRlUGxhbklkPTAwMDEzMTI1MTUmcm9vbVR5cGVJZD0wMDAwMDQ1NjM2JnJvb21zJTVCMCU1RC5jaGlsZEFnZXM9NSwxMCZyb29tcyU1QjAlNUQuY2hpbGRzPTInLFxyXG5cdFx0XHRcdFx0ZGF0YToge31cclxuXHRcdFx0XHR9KS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0XHRcdFx0XHRsb2FkUHJvbWlzZSA9IHVuZGVmaW5lZDtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGRhdGEpO1xyXG5cdFx0XHRcdFx0aWYoZGF0YS5zdGF0dXMgPT09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhLmRhdGEuZGF0YSk7XHJcblx0XHRcdFx0XHRcdGhvdGVscHJpY2UgPSBuZXcgSG90ZWxQcmljaW5nKGRhdGEuZGF0YS5kYXRhKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gbG9hZFByb21pc2U7XHJcblx0XHR9O1xyXG5cclxuXHRcdHJldHVybiB7XHJcblx0XHRcdGdldEFsbDogZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdHJldHVybiBsb2FkQWxsKCkudGhlbihmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gaG90ZWxwcmljZTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0fSk7IiwiYW5ndWxhci5tb2R1bGUoJ2ZpcnN0QXBwJylcclxuXHQuZmFjdG9yeSgnSG90ZWwnLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24gSG90ZWwoaG90ZWwpIHtcclxuXHRcdFx0YW5ndWxhci5leHRlbmQodGhpcywgaG90ZWwpO1xyXG5cdFx0XHRjb25zb2xlLmxvZyhob3RlbCk7XHJcblx0XHR9O1xyXG5cdH0pXHJcblx0LmZhY3RvcnkoJ0hvdGVsU2VydmljZScsIGZ1bmN0aW9uICgkcSwgJGh0dHAsIEhvdGVsKSB7XHJcblxyXG5cdFx0dmFyIGhvdGVsID0gW107XHJcblx0XHR2YXIgbG9hZFByb21pc2UgPSB1bmRlZmluZWQ7XHJcblx0XHR2YXIgbG9hZEFsbCA9IGZ1bmN0aW9uIChob3RlbGlkKSB7XHJcblx0XHRcdGlmIChob3RlbC5sb2FkUHJvbWlzZSA+IDApIHtcclxuXHRcdFx0XHRyZXR1cm4gJHEud2hlbihob3RlbCk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKF8uaXNVbmRlZmluZWQobG9hZFByb21pc2UpKSB7XHJcblx0XHRcdFx0bG9hZFByb21pc2UgPSAkaHR0cCh7XHJcblx0XHRcdFx0XHRtZXRob2Q6ICdHRVQnLFxyXG5cdFx0XHRcdFx0dXJsOiAnaHR0cDovL2hvbGlkYXlzZm9yYWxsLmluL3RyaXBzb2x1dGlvbnMvY29udGVudC9ob3RlbD9ob3RlbGlkPScrIGhvdGVsaWQsXHJcblx0XHRcdFx0XHRkYXRhOiB7fVxyXG5cdFx0XHRcdH0pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcclxuXHRcdFx0XHRcdGxvYWRQcm9taXNlID0gdW5kZWZpbmVkO1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coZGF0YSk7XHJcblx0XHRcdFx0XHRpZihkYXRhLnN0YXR1cyA9PT0gMjAwKSB7XHJcblx0XHRcdFx0XHRcdGhvdGVsID0gbmV3IEhvdGVsKGRhdGEuZGF0YS5kYXRhKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gbG9hZFByb21pc2U7XHJcblx0XHR9O1xyXG5cclxuXHRcdHJldHVybiB7XHJcblx0XHRcdGdldEFsbDogZnVuY3Rpb24gKGhvdGVsaWQpIHtcclxuXHRcdFx0XHRyZXR1cm4gbG9hZEFsbChob3RlbGlkKS50aGVuKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHJldHVybiBob3RlbDtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0fSk7Il19
